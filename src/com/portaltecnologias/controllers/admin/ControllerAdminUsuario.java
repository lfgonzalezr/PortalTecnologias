package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/usuario")
public class ControllerAdminUsuario {

	@RequestMapping(value = "/consultar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> consultarUsuarios() {
		return null;
	}

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> insertarUsuario() {
		return null;
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> actualizarUsuario() {
		return null;
	}

	@RequestMapping(value = "/borrar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> borrarUsuario() {
		return null;
	}
	
	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault() {
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}
