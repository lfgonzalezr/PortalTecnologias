package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.SeccionDto;
import com.portaltecnologias.models.service.admin.SeccionesServiceInterface;

@RestController
@RequestMapping("/admin/seccion")
public class ControllerAdminSeccion {

	@Autowired
	private SeccionesServiceInterface seccionesService;

	@RequestMapping(method = RequestMethod.POST, value = "/consultar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> consultarSecciones(@RequestBody SeccionDto seccionDto) {
		return new ResponseEntity<String>(seccionesService.consultarSecciones(seccionDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> insertarNuevaSeccion(@RequestBody SeccionDto seccionDto) {
		return new ResponseEntity<String>(seccionesService.insertarSeccion(seccionDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/actualizar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> actualizarSeccion(@RequestBody SeccionDto seccionDto) {
		return new ResponseEntity<String>(seccionesService.actualizarSeccion(seccionDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/borrar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> borrarSeccion(@RequestBody SeccionDto seccionDto) {
		return new ResponseEntity<String>(seccionesService.borrarSeccion(seccionDto), HttpStatus.OK);
	}
	
	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault(){
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}
