package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.ParrafosDto;
import com.portaltecnologias.models.service.admin.ParrafosServiceInterface;

@RestController
@RequestMapping("/admin/parrafo")
public class ControllerAdminParrafo {

	@Autowired
	private ParrafosServiceInterface parrafosService;

	@RequestMapping(method = RequestMethod.POST, value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> insertarParrafo(@RequestBody ParrafosDto parrafoDto) {
		return new ResponseEntity<String>(parrafosService.insertarParrafo(parrafoDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/actualizar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> actualizarParrafo(@RequestBody ParrafosDto parrafoDto) {
		return new ResponseEntity<String>(parrafosService.actualizarParrafo(parrafoDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/borrar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> borrarParrafo(@RequestBody ParrafosDto parrafoDto) {
		return new ResponseEntity<String>(parrafosService.borrarParrafo(parrafoDto), HttpStatus.OK);
	}

	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault() {
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}
