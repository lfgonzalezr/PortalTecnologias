package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.MateriaDto;
import com.portaltecnologias.models.service.admin.MateriasServiceInterface;

@RestController
@RequestMapping("/admin/materia")
public class ControllerAdminMateria {

	@Autowired
	private MateriasServiceInterface materiasService;

	@RequestMapping(method = RequestMethod.POST, value = "/consultar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> consultarMaterias(HttpServletRequest httpServletRequest) {
		String token = httpServletRequest.getHeader("token");
		return new ResponseEntity<String>(materiasService.consultarMaterias(token), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> insertarNuevaMateria(@RequestBody MateriaDto materiaDto) {
		return new ResponseEntity<String>(materiasService.insertarMateria(materiaDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/actualizar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> actualizarMateria(@RequestBody MateriaDto materiaDto) {
		return new ResponseEntity<String>(materiasService.actualizarMateria(materiaDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/borrar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> borrarMateria(@RequestBody MateriaDto materiaDto) {
		return new ResponseEntity<String>(materiasService.borrarMateria(materiaDto), HttpStatus.OK);
	}

	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault() {
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}
