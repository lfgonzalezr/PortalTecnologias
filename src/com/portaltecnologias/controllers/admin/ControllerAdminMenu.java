package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.MenuDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;
import com.portaltecnologias.models.service.admin.MenusServiceInterface;

@RestController
@RequestMapping("/admin/menu")
public class ControllerAdminMenu {

	@Autowired
	private MenusServiceInterface menuService;

	@RequestMapping(value = "/consultar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> consultarMenus() {
		return null;
	}

	@RequestMapping(value = "/crear", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> insertarMenu(@RequestBody MenuDto menuDto) {
		return null;
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> actualizarMenu(@RequestBody MenuDto menuDto) {
		return null;
	}

	@RequestMapping(value = "/borrar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> borrarActualizar(@RequestBody MenuDto menuDto) {
		return null;
	}

	@RequestMapping(value = "/consultar-menu-header", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> consultarMenusHeader(@RequestBody UsuarioDto usuarioDto) {
		return new ResponseEntity<String>(menuService.consultarMenusHeader(usuarioDto), HttpStatus.OK);
	}

	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault() {
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}