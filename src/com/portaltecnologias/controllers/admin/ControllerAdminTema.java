package com.portaltecnologias.controllers.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_CONTROLLER_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.TemasDto;
import com.portaltecnologias.models.service.admin.TemasServiceInterface;

@RestController
@RequestMapping("/admin/tema")
public class ControllerAdminTema {

	@Autowired
	private TemasServiceInterface temasService;

	@RequestMapping(method = RequestMethod.POST, value = "/consultar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> consultarTemas(@RequestBody TemasDto temasDto) {
		return new ResponseEntity<String>(temasService.consultarTemas(temasDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/crear", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> insertarTema(@RequestBody TemasDto temasDto) {
		return new ResponseEntity<String>(temasService.insertarTema(temasDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/actualizar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> actualizarTema(@RequestBody TemasDto temasDto) {
		return new ResponseEntity<String>(temasService.actualizarTema(temasDto), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/borrar", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody ResponseEntity<String> borrarTema(@RequestBody TemasDto temasDto) {
		return new ResponseEntity<String>(temasService.borrarTema(temasDto), HttpStatus.OK);
	}

	@RequestMapping("*")
	public @ResponseBody ResponseEntity<String> metodoDefault() {
		return new ResponseEntity<String>(JSON_CONTROLLER_EXC, HttpStatus.OK);
	}
}
