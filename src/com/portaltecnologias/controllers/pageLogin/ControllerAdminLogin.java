package com.portaltecnologias.controllers.pageLogin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.portaltecnologias.models.dto.common.UsuarioDto;
import com.portaltecnologias.models.service.login.LoginServiceInterface;

@RestController
@RequestMapping("/admin/login")
public class ControllerAdminLogin {

	@Autowired
	private LoginServiceInterface loginService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> login(@RequestBody UsuarioDto usuarioDto) {
		return new ResponseEntity<String>(loginService.login(usuarioDto), HttpStatus.OK);
	}

	@RequestMapping(value = "/registrar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> registrar(@RequestBody UsuarioDto usuarioDto) {
		return new ResponseEntity<String>(loginService.registrar(usuarioDto), HttpStatus.OK);
	}
}
