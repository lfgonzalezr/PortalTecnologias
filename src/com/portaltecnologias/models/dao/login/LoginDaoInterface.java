package com.portaltecnologias.models.dao.login;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

public interface LoginDaoInterface {

	public ResponseDto login(UsuarioDto usuarioDto);

	public ResponseDto registrar(UsuarioDto usuarioDto);

}
