package com.portaltecnologias.models.dao.login;

import static com.portaltecnologias.constants.ConstantesAdminCommon.CODE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.DATA;
import static com.portaltecnologias.constants.ConstantesAdminCommon.ERR_DAO;
import static com.portaltecnologias.constants.ConstantesAdminCommon.MSGE;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

import oracle.jdbc.OracleTypes;

@Repository
public class LoginDaoImpl implements LoginDaoInterface {

	@Override
	public ResponseDto login(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.LOGIN_USUARIO(?,?,?,?,?)}");
			callableStatement.setString("CORREO_IN", usuarioDto.getCorreo());
			callableStatement.setString("PASSWORD_IN", usuarioDto.getPassword());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			ResultSet resultSet = (ResultSet) callableStatement.getObject(DATA);
			UsuarioDto usuario = new UsuarioDto();
			if (resultSet != null) {
				while (resultSet.next()) {
					usuario.setNombreUsuario(resultSet.getString("FC_NOMBRE"));
					usuario.setCorreo(resultSet.getString("FC_CORREO"));
					usuario.setPassword(resultSet.getString("TOKEN"));
				}

			}
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(usuario);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto registrar(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.REGISTRAR_USUARIO(?,?,?,?,?,?,?)}");
			callableStatement.setString("NOMBRE", usuarioDto.getNombreUsuario());
			callableStatement.setString("CORREO", usuarioDto.getCorreo());
			callableStatement.setString("PASSWORD_IN", usuarioDto.getPassword());
			callableStatement.setInt("STATUS", 1);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			ResultSet resultSet = (ResultSet) callableStatement.getObject(DATA);
			UsuarioDto usuario = new UsuarioDto();
			if (resultSet != null) {
				while (resultSet.next()) {
					usuario.setNombreUsuario(resultSet.getString("FC_NOMBRE"));
					usuario.setCorreo(resultSet.getString("FC_CORREO"));
					usuario.setPassword(resultSet.getString("TOKEN"));
				}

			}
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(usuario);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

}
