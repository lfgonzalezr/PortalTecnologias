package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.CODE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.DATA;
import static com.portaltecnologias.constants.ConstantesAdminCommon.ERR_DAO;
import static com.portaltecnologias.constants.ConstantesAdminCommon.MSGE;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.RolDto;
import com.portaltecnologias.models.mappers.admin.RolDtoMapper;

import oracle.jdbc.OracleTypes;

@Repository
public class RolesDaoImpl implements RolesDaoInterface {

	@Override
	public ResponseDto consultarRoles() {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		RolDtoMapper mapper = new RolDtoMapper();
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_ROLES.CONSULTAR_ROLES(?,?,?)}");
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarRol(RolDto rolDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_ROLES.INSERTAR_ROL(?,?,?,?,?,?)}");
			callableStatement.setString("NOMBRE", rolDto.getNombre());
			callableStatement.setString("DESCRIPCION", rolDto.getDescripcion());
			callableStatement.setInt("STATUS", rolDto.getStatus());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto actualizarRol(RolDto rolDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_ROLES.ACTUALIZAR_ROL(?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_ROL", rolDto.getIdRol());
			callableStatement.setString("NOMBRE", rolDto.getNombre());
			callableStatement.setString("DESCRIPCION", rolDto.getDescripcion());
			callableStatement.setInt("STATUS", rolDto.getStatus());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto borrarRol(RolDto rolDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_ROLES.BORRAR_ROL(?,?,?,?)}");
			callableStatement.setInt("ID_ROL", rolDto.getIdRol());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

}
