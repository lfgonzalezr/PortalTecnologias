package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

public interface UsuariosDaoInterface {

	public ResponseDto consultarUsuarios();

	public ResponseDto loginUsuario(UsuarioDto usuarioDto);

	public ResponseDto registrarUsuario(UsuarioDto usuarioDto);

	public ResponseDto actualizarUsuario(UsuarioDto usuarioDto);

	public ResponseDto borrarUsuario(UsuarioDto usuarioDto);
}
