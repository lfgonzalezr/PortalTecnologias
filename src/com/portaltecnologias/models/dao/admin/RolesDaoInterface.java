package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.RolDto;

public interface RolesDaoInterface {

	public ResponseDto consultarRoles();

	public ResponseDto insertarRol(RolDto rolDto);

	public ResponseDto actualizarRol(RolDto rolDto);

	public ResponseDto borrarRol(RolDto rolDto);
}
