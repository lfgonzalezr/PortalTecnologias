package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.ParrafosDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.mappers.admin.ParrafosDtoMapper;

import oracle.jdbc.OracleTypes;

import static com.portaltecnologias.constants.ConstantesAdminCommon.CODE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.DATA;
import static com.portaltecnologias.constants.ConstantesAdminCommon.ERR_DAO;
import static com.portaltecnologias.constants.ConstantesAdminCommon.MSGE;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ParrafosDaoImpl implements ParrafosDaoInterface {

	@Override
	public ResponseDto consultarParrafos(int idMateria, int idSeccion, int idTema) {
		ResponseDto responseDto = new ResponseDto();
		try {
			ResultSet resultSet = null;
			int codigo;
			String mensaje;
			ParrafosDtoMapper mapper = new ParrafosDtoMapper();
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_PARRAFOS.CONSULTAR_PARRAFOS(?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", idMateria);
			callableStatement.setInt("ID_SECCION", idSeccion);
			callableStatement.setInt("ID_TEMA", idTema);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarParrafo(ParrafosDto parrafo) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_PARRAFOS.INSERTAR_PARRAFO(?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", parrafo.getIdMateria());
			callableStatement.setInt("ID_SECCION", parrafo.getIdSeccion());
			callableStatement.setInt("ID_TEMA", parrafo.getIdTema());
			callableStatement.setInt("ORDER_IN", parrafo.getOrder());
			callableStatement.setString("TITULO", parrafo.getTitulo());
			callableStatement.setString("SUBTITULO", parrafo.getSubtitulo());
			callableStatement.setString("TEXTO_PARRAFO", parrafo.getTextoParrafo());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarArrayParrafos(List<ParrafosDto> lista) {
		ResponseDto response = new ResponseDto();
		return response;
	}

	@Override
	public ResponseDto consultarParrafoId(ParrafosDto parrafo) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public ResponseDto actualizarParrafoId(ParrafosDto parrafo) {

		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_PARRAFOS.ACTUALIZAR_PARRAFO(?,?,?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", parrafo.getIdMateria());
			callableStatement.setInt("ID_SECCION", parrafo.getIdSeccion());
			callableStatement.setInt("ID_TEMA", parrafo.getIdTema());
			callableStatement.setInt("ID_PARRAFO", parrafo.getIdParrafo());
			callableStatement.setInt("ORDER_IN", parrafo.getOrder());
			callableStatement.setString("TITULO", parrafo.getTitulo());
			callableStatement.setString("SUBTITULO", parrafo.getSubtitulo());
			callableStatement.setString("TEXTO_PARRAFO", parrafo.getTextoParrafo());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;

	}

	@Override
	public ResponseDto borrarParrafo(ParrafosDto parrafo) {

		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_PARRAFOS.BORRAR_PARRAFO(?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", parrafo.getIdMateria());
			callableStatement.setInt("ID_SECCION", parrafo.getIdSeccion());
			callableStatement.setInt("ID_TEMA", parrafo.getIdTema());
			callableStatement.setInt("ID_PARRAFO", parrafo.getIdParrafo());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;

	}
}
