package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.SeccionDto;
import java.util.List;

public interface SeccionesDaoInterface {

    public ResponseDto consultarSecciones(int idMateria);

    public ResponseDto consultarSeccionId(SeccionDto seccion);

    public ResponseDto insertarSeccion(SeccionDto seccion);

    public ResponseDto actualizarSeccion(SeccionDto seccion);

    public ResponseDto borrarSeccion(SeccionDto seccion);

    public ResponseDto insertarArraySecciones(List<SeccionDto> lista);
}
