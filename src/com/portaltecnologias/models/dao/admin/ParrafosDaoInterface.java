package com.portaltecnologias.models.dao.admin;

import java.util.List;

import com.portaltecnologias.models.dto.common.ParrafosDto;
import com.portaltecnologias.models.dto.common.ResponseDto;

public interface ParrafosDaoInterface {

    public ResponseDto consultarParrafos(int idMateria, int idSeccion, int idTema);

    public ResponseDto insertarParrafo(ParrafosDto parrafo);

    public ResponseDto consultarParrafoId(ParrafosDto parrafo);

    public ResponseDto actualizarParrafoId(ParrafosDto parrafo);

    public ResponseDto borrarParrafo(ParrafosDto parrafo);

    public ResponseDto insertarArrayParrafos(List<ParrafosDto> lista);
}
