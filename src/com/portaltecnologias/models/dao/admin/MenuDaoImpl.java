package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.CODE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.DATA;
import static com.portaltecnologias.constants.ConstantesAdminCommon.ERR_DAO;
import static com.portaltecnologias.constants.ConstantesAdminCommon.MSGE;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.portaltecnologias.models.dto.common.MenuDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;
import com.portaltecnologias.models.mappers.admin.MenusDtoMapper;

import oracle.jdbc.OracleTypes;

@Repository
public class MenuDaoImpl implements MenusDaoInterface {

	@Override
	public ResponseDto consultarMenus() {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		MenusDtoMapper mapper = new MenusDtoMapper();
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MENU.CONSULTAR_MENU(?,?,?)}");
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto consultarMenusPorUsuario(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		MenusDtoMapper mapper = new MenusDtoMapper();
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MENU.CONSULTAR_MENUS_POR_USUARIO(?,?,?,?)}");
			callableStatement.setString("CORREO", usuarioDto.getCorreo());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;

	}

	@Override
	public ResponseDto insertarMenu(MenuDto menuDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MENU.INSERTAR_MENU(?,?,?,?,?,?,?,?)}");
			callableStatement.setString("NOMBRE", menuDto.getNombre());
			callableStatement.setString("DESCRIPCION", menuDto.getDescripcion());
			callableStatement.setInt("STATUS", menuDto.getStatus());
			callableStatement.setString("ICONO", menuDto.getIcono());
			callableStatement.setInt("TIPO_ELEMENTO", menuDto.getTipoElemento());
			callableStatement.setString("URI", menuDto.getUri());
			callableStatement.setInt("ORDEN", menuDto.getOrden());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto actualizarMenu(MenuDto menuDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MENU.ACTUALIZAR_MENU(?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MENU", menuDto.getIdMenu());
			callableStatement.setString("NOMBRE", menuDto.getNombre());
			callableStatement.setString("DESCRIPCION", menuDto.getDescripcion());
			callableStatement.setInt("STATUS", menuDto.getStatus());
			callableStatement.setString("ICONO", menuDto.getIcono());
			callableStatement.setInt("TIPO_ELEMENTO", menuDto.getTipoElemento());
			callableStatement.setString("URI", menuDto.getUri());
			callableStatement.setInt("ORDEN", menuDto.getOrden());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto borrarMenu(MenuDto menuDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MENU.BORRAR_MENU(?,?,?,?)}");
			callableStatement.setInt("ID_MENU", menuDto.getIdMenu());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}
}
