package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.TemasDto;
import java.util.List;

public interface TemasDaoInterface {

    public ResponseDto consultarTemas(int idMateria, int idSeccion);

    public ResponseDto consultarTemaId(TemasDto tema);

    public ResponseDto insertarTema(TemasDto tema);

    public ResponseDto actualizarTema(TemasDto tema);

    public ResponseDto borrarTema(TemasDto tema);

    public ResponseDto insertarArrayTemas(List<TemasDto> lista);
}
