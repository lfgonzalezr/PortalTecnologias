package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.TemasDto;
import com.portaltecnologias.models.mappers.admin.TemasDtoMapper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;
import org.springframework.stereotype.Repository;

@Repository
public class TemasDaoImpl implements TemasDaoInterface {

	@Override
	public ResponseDto consultarTemas(int idMateria, int idSeccion) {
		ResponseDto response = new ResponseDto();
		try {
			ResultSet resultSet = null;
			TemasDtoMapper mapper = new TemasDtoMapper();
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_TEMAS.CONSULTAR_TEMAS(?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", idMateria);
			callableStatement.setInt("ID_SECCION", idSeccion);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			response.setCodigo(codigo);
			response.setMensage(mensaje);
			response.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			response.setCodigo(ERR_DAO);
			response.setMensage("Fallo en la conexion a BD - SQLException");
			response.setContenido(null);
		}
		return response;
	}

	@Override
	public ResponseDto insertarTema(TemasDto tema) {
		ResponseDto response = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_TEMAS.INSERTAR_TEMA(?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", tema.getIdMateria());
			callableStatement.setInt("ID_SECCION", tema.getIdSeccion());
			callableStatement.setString("TITULO", tema.getTitulo());
			callableStatement.setString("SUBTITULO", tema.getSubtitulo());
			callableStatement.setInt("ORDER_IN", tema.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			response.setCodigo(codigo);
			response.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			response.setCodigo(ERR_DAO);
			response.setMensage("Fallo en la conexion a BD - SQLException");
			response.setContenido(null);
		}
		return response;
	}

	@Override
	public ResponseDto consultarTemaId(TemasDto tema) {
		ResponseDto response = new ResponseDto();
		try {
			TemasDtoMapper mapper = new TemasDtoMapper();
			ResultSet resultSet = null;
			List<TemasDto> lista = new ArrayList<TemasDto>();
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_TEMAS.CONSULTAR_TEMA_ID(?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", tema.getIdMateria());
			callableStatement.setInt("ID_SECCION", tema.getIdSeccion());
			callableStatement.setInt("ID_TEMA", tema.getIdTema());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			response.setCodigo(codigo);
			response.setMensage(mensaje);
			lista = mapper.mapRow(resultSet, 1);
			response.setContenido(lista.get(0));
			connection.close();
		} catch (SQLException e) {
			response.setCodigo(ERR_DAO);
			response.setMensage("Fallo en la conexion a BD - SQLException");
			response.setContenido(null);
		}
		return response;
	}

	@Override
	public ResponseDto actualizarTema(TemasDto tema) {
		ResponseDto response = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_TEMAS.ACTUALIZAR_TEMA(?,?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", tema.getIdMateria());
			callableStatement.setInt("ID_SECCION", tema.getIdSeccion());
			callableStatement.setInt("ID_TEMA", tema.getIdTema());
			callableStatement.setString("TITULO", tema.getTitulo());
			callableStatement.setString("SUBTITULO", tema.getSubtitulo());
			callableStatement.setInt("ORDER_IN", tema.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			response.setCodigo(codigo);
			response.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			response.setCodigo(ERR_DAO);
			response.setMensage("Fallo en la conexion a BD - SQLException");
			response.setContenido(null);
		}
		return response;
	}

	@Override
	public ResponseDto borrarTema(TemasDto tema) {
		ResponseDto response = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_TEMAS.BORRAR_TEMA(?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", tema.getIdMateria());
			callableStatement.setInt("ID_SECCION", tema.getIdSeccion());
			callableStatement.setInt("ID_TEMA", tema.getIdTema());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			response.setCodigo(codigo);
			response.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			response.setCodigo(ERR_DAO);
			response.setMensage("Fallo en la conexion a BD - SQLException");
			response.setContenido(null);
		}
		return response;
	}

	@Override
	public ResponseDto insertarArrayTemas(List<TemasDto> lista) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
