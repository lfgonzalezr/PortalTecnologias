package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.SeccionDto;
import com.portaltecnologias.models.mappers.admin.SeccionesDtoMapper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import oracle.jdbc.OracleTypes;
import org.springframework.stereotype.Repository;

@Repository
public class SeccionesDaoImpl implements SeccionesDaoInterface {

	@Override
	public ResponseDto consultarSecciones(int idMateria) {
		ResponseDto responseDto = new ResponseDto();
		try {
			ResultSet resultSet = null;
			int codigo;
			String mensaje;
			SeccionesDtoMapper mapper = new SeccionesDtoMapper();
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_SECCIONES.CONSULTAR_SECCIONES(?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", idMateria);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarSeccion(SeccionDto seccion) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_SECCIONES.INSERTAR_SECCION(?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", seccion.getIdMateria());
			callableStatement.setString("TITULO", seccion.getTitulo());
			callableStatement.setString("SUBTITULO", seccion.getSubtitulo());
			callableStatement.setInt("ORDER_IN", seccion.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto consultarSeccionId(SeccionDto seccion) {
		ResponseDto responseDto = new ResponseDto();
		try {
			ResultSet resultSet = null;
			List<SeccionDto> lista = new ArrayList<SeccionDto>();
			int codigo;
			String mensaje;
			SeccionesDtoMapper mapper = new SeccionesDtoMapper();
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_SECCIONES.CONSULTAR_SECCIONES(?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", seccion.getIdMateria());
			callableStatement.setInt("ID_SECCION", seccion.getIdMateria());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			lista = mapper.mapRow(resultSet, 1);
			responseDto.setContenido(lista.get(0));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto actualizarSeccion(SeccionDto seccion) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_SECCIONES.ACTUALIZAR_SECCION(?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_MATERIA", seccion.getIdMateria());
			callableStatement.setInt("ID_SECCION", seccion.getIdSeccion());
			callableStatement.setString("TITULO", seccion.getTitulo());
			callableStatement.setString("SUBTITULO", seccion.getSubtitulo());
			callableStatement.setInt("ORDER_IN", seccion.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto borrarSeccion(SeccionDto seccion) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_SECCIONES.BORRAR_SECCION(?,?,?,?,?)}");

			callableStatement.setInt("ID_MATERIA", seccion.getIdMateria());
			callableStatement.setInt("ID_SECCION", seccion.getIdSeccion());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarArraySecciones(List<SeccionDto> lista) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
