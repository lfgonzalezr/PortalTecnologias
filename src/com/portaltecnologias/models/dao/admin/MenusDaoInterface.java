package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.MenuDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

public interface MenusDaoInterface {

	public ResponseDto consultarMenus();

	public ResponseDto consultarMenusPorUsuario(UsuarioDto usuarioDto);

	public ResponseDto insertarMenu(MenuDto menuDto);

	public ResponseDto actualizarMenu(MenuDto menuDto);

	public ResponseDto borrarMenu(MenuDto menuDto);

}
