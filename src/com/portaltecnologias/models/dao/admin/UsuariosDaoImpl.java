package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.CODE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.DATA;
import static com.portaltecnologias.constants.ConstantesAdminCommon.ERR_DAO;
import static com.portaltecnologias.constants.ConstantesAdminCommon.MSGE;
import static com.portaltecnologias.constants.ConstantesAdminCommon.SUCCESS;
import static com.portaltecnologias.utils.UtilConnection.getConnection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;
import com.portaltecnologias.models.mappers.admin.UsuariosDtoMapper;

import oracle.jdbc.OracleTypes;

@Repository
public class UsuariosDaoImpl implements UsuariosDaoInterface {

	@Override
	public ResponseDto consultarUsuarios() {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		UsuariosDtoMapper mapper = new UsuariosDtoMapper();
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.CONSULTAR_USUARIOS(?,?,?)}");
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto loginUsuario(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.LOGIN_USUARIO(?,?,?,?,?)}");
			callableStatement.setString("CORREO_IN", usuarioDto.getCorreo());
			callableStatement.setString("PASSWORD_IN", usuarioDto.getPassword());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			if (codigo == SUCCESS) {
				resultSet = (ResultSet) callableStatement.getObject(DATA);
				if (resultSet != null) {
					UsuarioDto credentialUser = new UsuarioDto();
					while (resultSet.next()) {
						credentialUser.setNombreUsuario(resultSet.getString("FC_NOMBRE"));
						credentialUser.setCorreo(resultSet.getString("FC_CORREO"));
						credentialUser.setToken(resultSet.getString("TOKEN"));
					}
					responseDto.setContenido(credentialUser);
				}
			}
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto registrarUsuario(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		ResultSet resultSet = null;
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.REGISTRAR_USUARIO(?,?,?,?,?,?,?)}");
			callableStatement.setString("NOMBRE", usuarioDto.getNombreUsuario());
			callableStatement.setString("CORREO", usuarioDto.getCorreo());
			callableStatement.setInt("STATUS", 1);
			callableStatement.setString("PASSWORD_IN", usuarioDto.getPassword());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			if (codigo == SUCCESS) {
				resultSet = (ResultSet) callableStatement.getObject(DATA);
				if (resultSet != null) {
					UsuarioDto credentialUser = new UsuarioDto();
					while (resultSet.next()) {
						credentialUser.setNombreUsuario(resultSet.getString("FC_NOMBRE"));
						credentialUser.setCorreo(resultSet.getString("FC_CORREO"));
						credentialUser.setToken(resultSet.getString("TOKEN"));
					}
					responseDto.setContenido(credentialUser);
				}
			}
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto actualizarUsuario(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.ACTUALIZAR_USUARIO(?,?,?,?,?,?,?,?)}");
			callableStatement.setInt("ID_USUARIO", usuarioDto.getIdUsuario());
			callableStatement.setString("NOMBRE", usuarioDto.getNombreUsuario());
			callableStatement.setString("CORREO", usuarioDto.getCorreo());
			callableStatement.setInt("STATUS", 1);
			callableStatement.setString("PASSWORD_IN", usuarioDto.getPassword());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto borrarUsuario(UsuarioDto usuarioDto) {
		ResponseDto responseDto = new ResponseDto();
		int codigo;
		String mensaje;
		Connection connection;
		try {
			connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_USUARIOS.BORRAR_USUARIO(?,?,?,?,?)}");
			callableStatement.setString("CORREO", usuarioDto.getCorreo());
			callableStatement.setString("CONTRASENA", usuarioDto.getPassword());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

}
