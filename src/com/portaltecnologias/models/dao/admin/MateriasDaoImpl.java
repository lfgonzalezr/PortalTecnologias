/**
 *
 */
package com.portaltecnologias.models.dao.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;
import static com.portaltecnologias.utils.UtilConnection.getConnection;
import com.portaltecnologias.models.dto.common.MateriaDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.mappers.admin.MateriasDtoMapper;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;
import org.springframework.stereotype.Repository;

/**
 * @author lfgonzalezr
 *
 */
@Repository
public class MateriasDaoImpl implements MateriasDaoInterface {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.portaltecnologias.models.dao.pageAdmin.PageAdminDaoInterface#
	 * buscarMaterias()
	 */
	@Override
	public ResponseDto buscarMaterias() {
		ResponseDto responseDto = new ResponseDto();
		try {
			MateriasDtoMapper mapper = new MateriasDtoMapper();
			ResultSet resultSet = null;
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MATERIAS.CONSULTAR_MATERIAS(?,?,?)}");

			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			responseDto.setContenido(mapper.mapRow(resultSet, 1));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarMateria(MateriaDto materia) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MATERIAS.INSERTAR_MATERIA(?,?,?,?,?,?,?,?)}");

			callableStatement.setInt("ID_MATERIA", materia.getIdMateria());
			callableStatement.setString("TITULO", materia.getTitulo());
			callableStatement.setString("DESCRIPCION", materia.getDescripcion());
			callableStatement.setString("URL_IN", materia.getUrl());
			callableStatement.setInt("ORDER_IN", materia.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto buscarMateriaId(int idMateria) {
		ResponseDto responseDto = new ResponseDto();
		try {
			MateriasDtoMapper mapper = new MateriasDtoMapper();
			ResultSet resultSet = null;
			List<MateriaDto> lista = new ArrayList<MateriaDto>();
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MATERIAS.BUSCAR_MATERIA_ID(?,?,?,?)}");

			callableStatement.setInt("ID_MATERIA", idMateria);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			resultSet = (ResultSet) callableStatement.getObject(DATA);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			lista = mapper.mapRow(resultSet, 1);
			responseDto.setContenido(lista.get(0));
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto actualizarMateria(MateriaDto materia) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MATERIAS.ACTUALIZAR_MATERIA(?,?,?,?,?,?,?,?)}");

			callableStatement.setInt("ID_MATERIA", materia.getIdMateria());
			callableStatement.setString("TITULO", materia.getTitulo());
			callableStatement.setString("DESCRIPCION", materia.getDescripcion());
			callableStatement.setString("URL_IN", materia.getUrl());
			callableStatement.setInt("ORDER_IN", materia.getOrder());
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto borrarMateria(int idMateria) {
		ResponseDto responseDto = new ResponseDto();
		try {
			int codigo;
			String mensaje;
			Connection connection = getConnection();
			CallableStatement callableStatement = connection.prepareCall("{call PKG_ADMIN_MATERIAS.BORRAR_MATERIA(?,?,?,?)}");

			callableStatement.setInt("ID_MATERIA", idMateria);
			callableStatement.registerOutParameter(CODE, java.sql.Types.NUMERIC);
			callableStatement.registerOutParameter(MSGE, java.sql.Types.VARCHAR);
			callableStatement.registerOutParameter(DATA, OracleTypes.CURSOR);
			// Ejecucion de SP
			callableStatement.execute();
			// Recepcion de respuesta
			codigo = callableStatement.getInt(CODE);
			mensaje = callableStatement.getString(MSGE);
			responseDto.setCodigo(codigo);
			responseDto.setMensage(mensaje);
			connection.close();
		} catch (SQLException e) {
			responseDto.setCodigo(ERR_DAO);
			responseDto.setMensage("Fallo en la conexion a BD - SQLException");
			responseDto.setContenido(null);
		}
		return responseDto;
	}

	@Override
	public ResponseDto insertarArrayMaterias(List<MateriaDto> lista) {
		ResponseDto responseDto = new ResponseDto();
		return responseDto;
	}
}
