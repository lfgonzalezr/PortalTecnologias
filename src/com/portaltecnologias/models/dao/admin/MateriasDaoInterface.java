package com.portaltecnologias.models.dao.admin;

import com.portaltecnologias.models.dto.common.MateriaDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import java.util.List;

public interface MateriasDaoInterface {

    public ResponseDto buscarMaterias();

    public ResponseDto insertarMateria(MateriaDto materia);

    public ResponseDto buscarMateriaId(int idMatera);

    public ResponseDto actualizarMateria(MateriaDto materia);

    public ResponseDto borrarMateria(int idMateria);
    
    public ResponseDto insertarArrayMaterias(List<MateriaDto> lista);
}