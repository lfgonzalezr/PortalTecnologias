package com.portaltecnologias.models.dto.common;

public class MenuDto {
	private int idMenu;
	private String nombre;
	private String descripcion;
	private String uri;
	private int status;
	private String icono;
	private int tipoElemento;
	private int orden;

	public final int getIdMenu() {
		return idMenu;
	}

	public final void setIdMenu(int idMenu) {
		this.idMenu = idMenu;
	}

	public final String getNombre() {
		return nombre;
	}

	public final void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public final String getDescripcion() {
		return descripcion;
	}

	public final void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public final String getUri() {
		return uri;
	}

	public final void setUri(String uri) {
		this.uri = uri;
	}

	public final int getStatus() {
		return status;
	}

	public final void setStatus(int status) {
		this.status = status;
	}

	public final String getIcono() {
		return icono;
	}

	public final void setIcono(String icono) {
		this.icono = icono;
	}

	public final int getTipoElemento() {
		return tipoElemento;
	}

	public final void setTipoElemento(int tipoElemento) {
		this.tipoElemento = tipoElemento;
	}

	public final int getOrden() {
		return orden;
	}

	public final void setOrden(int orden) {
		this.orden = orden;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((icono == null) ? 0 : icono.hashCode());
		result = prime * result + idMenu;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + orden;
		result = prime * result + status;
		result = prime * result + tipoElemento;
		result = prime * result + ((uri == null) ? 0 : uri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MenuDto other = (MenuDto) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (icono == null) {
			if (other.icono != null)
				return false;
		} else if (!icono.equals(other.icono))
			return false;
		if (idMenu != other.idMenu)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (orden != other.orden)
			return false;
		if (status != other.status)
			return false;
		if (tipoElemento != other.tipoElemento)
			return false;
		if (uri == null) {
			if (other.uri != null)
				return false;
		} else if (!uri.equals(other.uri))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MenuDto [idMenu=" + idMenu + ", nombre=" + nombre + ", descripcion=" + descripcion + ", uri=" + uri + ", status=" + status + ", icono=" + icono + ", tipoElemento=" + tipoElemento
				+ ", orden=" + orden + "]";
	}
}
