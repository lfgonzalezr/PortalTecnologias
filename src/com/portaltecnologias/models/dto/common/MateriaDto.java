package com.portaltecnologias.models.dto.common;

public class MateriaDto {
	private Integer idMateria;
	private String titulo;
	private String descripcion;
	private String url;
	private Integer order;

	public MateriaDto() {

	}

	public MateriaDto(String titulo) {
		this.idMateria = 0;
		this.titulo = titulo;
		this.descripcion = "Opcion para agregar nueva materia";
		this.url = "";
		this.order = 0;
	}

	public MateriaDto(Integer idMateria, String titulo, String descripcion, String url, Integer order) {
		this.idMateria = idMateria;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.url = url;
		this.order = order;
	}

	/**
	 * @return the idMateria
	 */
	public Integer getIdMateria() {
		return idMateria;
	}

	/**
	 * @param idMateria the idMateria to set
	 */
	public void setIdMateria(Integer idMateria) {
		this.idMateria = idMateria;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{idMateria: " + idMateria + ", titulo: " + titulo + ", descripcion: " + descripcion + ", url: " + url + ", order: " + order + "}";
	}
}
