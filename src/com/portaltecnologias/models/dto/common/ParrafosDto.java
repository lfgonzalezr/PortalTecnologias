package com.portaltecnologias.models.dto.common;

public class ParrafosDto {
	private Integer idParrafo;
	private Integer idTema;
	private Integer idSeccion;
	private Integer idMateria;
	private String titulo;
	private String subtitulo;
	private String textoParrafo;
	private Integer order;

	/**
	 * @return the idParrafo
	 */
	public Integer getIdParrafo() {
		return idParrafo;
	}

	/**
	 * @param idParrafo
	 *            the idParrafo to set
	 */
	public void setIdParrafo(Integer idParrafo) {
		this.idParrafo = idParrafo;
	}

	/**
	 * @return the idTema
	 */
	public Integer getIdTema() {
		return idTema;
	}

	/**
	 * @param idTema
	 *            the idTema to set
	 */
	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}

	/**
	 * @return the idSeccion
	 */
	public Integer getIdSeccion() {
		return idSeccion;
	}

	/**
	 * @param idSeccion
	 *            the idSeccion to set
	 */
	public void setIdSeccion(Integer idSeccion) {
		this.idSeccion = idSeccion;
	}

	/**
	 * @return the idMateria
	 */
	public Integer getIdMateria() {
		return idMateria;
	}

	/**
	 * @param idMateria
	 *            the idMateria to set
	 */
	public void setIdMateria(Integer idMateria) {
		this.idMateria = idMateria;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo
	 *            the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the subtitulo
	 */
	public String getSubtitulo() {
		return subtitulo;
	}

	/**
	 * @param subtitulo
	 *            the subtitulo to set
	 */
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	/**
	 * @return the textoParrafo
	 */
	public String getTextoParrafo() {
		return textoParrafo;
	}

	/**
	 * @param textoParrafo
	 *            the textoParrafo to set
	 */
	public void setTextoParrafo(String textoParrafo) {
		this.textoParrafo = textoParrafo;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{idParrafo: " + idParrafo + ", idTema: " + idTema + ", idSeccion: " + idSeccion + ", idMateria: " + idMateria + ", titulo: " + titulo + ", subtitulo: " + subtitulo + ", textoParrafo: " + textoParrafo + ", order: " + order + "}";
	}
}
