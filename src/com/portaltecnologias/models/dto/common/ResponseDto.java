/**
 * 
 */
package com.portaltecnologias.models.dto.common;

/**
 * @author lfgonzalezr
 *
 */
public class ResponseDto {
	private int codigo;
	private String mensage;
	private Object contenido;

	public ResponseDto() {

	}

	public ResponseDto(int codigo, String mensage, Object contenido) {
		this.codigo = codigo;
		this.mensage = mensage;
		this.contenido = contenido;
	}

	/**
	 * @return the codigo
	 */
	public int getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the mensage
	 */
	public String getMensage() {
		return mensage;
	}

	/**
	 * @param mensage the mensage to set
	 */
	public void setMensage(String mensage) {
		this.mensage = mensage;
	}

	/**
	 * @return the contenido
	 */
	public Object getContenido() {
		return contenido;
	}

	/**
	 * @param contenido the contenido to set
	 */
	public void setContenido(Object contenido) {
		this.contenido = contenido;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{codigo: " + codigo + ", mensage: " + mensage + ", contenido: " + contenido + "}";
	}
}
