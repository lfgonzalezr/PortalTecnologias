package com.portaltecnologias.models.dto.common;

public class SeccionDto {
	private Integer idSeccion;
	private Integer idMateria;
	private String titulo;
	private String subtitulo;
	private Integer order;

	public SeccionDto() {

	}

	public SeccionDto(String titulo, Integer idMateria, Integer idSeccion, Integer order) {
		this.titulo = titulo;
		this.idMateria = idMateria;
		this.idSeccion = idSeccion;
		this.order = order;
	}

	/**
	 * @return the idSeccion
	 */
	public Integer getIdSeccion() {
		return idSeccion;
	}

	/**
	 * @param idSeccion the idSeccion to set
	 */
	public void setIdSeccion(Integer idSeccion) {
		this.idSeccion = idSeccion;
	}

	/**
	 * @return the idMateria
	 */
	public Integer getIdMateria() {
		return idMateria;
	}

	/**
	 * @param idMateria the idMateria to set
	 */
	public void setIdMateria(Integer idMateria) {
		this.idMateria = idMateria;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the subtitulo
	 */
	public String getSubtitulo() {
		return subtitulo;
	}

	/**
	 * @param subtitulo the subtitulo to set
	 */
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{idSeccion: " + idSeccion + ", idMateria: " + idMateria + ", titulo: " + titulo + ", subtitulo: " + subtitulo + ", order: " + order + "}";
	}
}
