package com.portaltecnologias.models.dto.common;

public class TemasDto {
	private Integer idTema;
	private Integer idSeccion;
	private Integer idMateria;
	private String titulo;
	private String subtitulo;
	private Integer order;

	public TemasDto() {

	}

	public TemasDto(String titulo, Integer idMateria, Integer idSeccion, Integer idTema) {
		this.titulo = titulo;
		this.idMateria = idMateria;
		this.idSeccion = idSeccion;
		this.idTema = idTema;
	}

	public TemasDto(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the idTema
	 */
	public Integer getIdTema() {
		return idTema;
	}

	/**
	 * @param idTema the idTema to set
	 */
	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}

	/**
	 * @return the idSeccion
	 */
	public Integer getIdSeccion() {
		return idSeccion;
	}

	/**
	 * @param idSeccion the idSeccion to set
	 */
	public void setIdSeccion(Integer idSeccion) {
		this.idSeccion = idSeccion;
	}

	/**
	 * @return the idMateria
	 */
	public Integer getIdMateria() {
		return idMateria;
	}

	/**
	 * @param idMateria the idMateria to set
	 */
	public void setIdMateria(Integer idMateria) {
		this.idMateria = idMateria;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the subtitulo
	 */
	public String getSubtitulo() {
		return subtitulo;
	}

	/**
	 * @param subtitulo the subtitulo to set
	 */
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{idTema: " + idTema + ", idSeccion: " + idSeccion + ", idMateria: " + idMateria + ", titulo: " + titulo + ", subtitulo: " + subtitulo + ", order: " + order + "}";
	}
}
