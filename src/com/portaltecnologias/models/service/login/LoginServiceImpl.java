package com.portaltecnologias.models.service.login;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_PROCCESSING_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.UsuariosDaoInterface;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

@Service
public class LoginServiceImpl implements LoginServiceInterface {

	@Autowired
	private UsuariosDaoInterface loginDao;

	@Override
	public String login(UsuarioDto usuarioDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = loginDao.loginUsuario(usuarioDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String registrar(UsuarioDto usuarioDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = loginDao.registrarUsuario(usuarioDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

}
