package com.portaltecnologias.models.service.login;

import com.portaltecnologias.models.dto.common.UsuarioDto;

public interface LoginServiceInterface {

	public String login(UsuarioDto usuarioDto);

	public String registrar(UsuarioDto usuarioDto);

}
