package com.portaltecnologias.models.service.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_PROCCESSING_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.MenusDaoInterface;
import com.portaltecnologias.models.dto.common.MenuDto;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

@Service
public class MenusServiceImpl implements MenusServiceInterface {

	@Autowired
	private MenusDaoInterface menuDao;

	@Override
	public String consultarMenus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String insertarMenu(MenuDto menuDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String actualizarMenu(MenuDto menuDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String borrarMenu(MenuDto menuDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String consultarMenusHeader(UsuarioDto usuarioDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = menuDao.consultarMenusPorUsuario(usuarioDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

}
