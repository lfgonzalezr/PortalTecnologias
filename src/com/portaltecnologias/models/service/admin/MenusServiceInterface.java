package com.portaltecnologias.models.service.admin;

import com.portaltecnologias.models.dto.common.MenuDto;
import com.portaltecnologias.models.dto.common.UsuarioDto;

public interface MenusServiceInterface {

	public String consultarMenus();

	public String insertarMenu(MenuDto menuDto);

	public String actualizarMenu(MenuDto menuDto);

	public String borrarMenu(MenuDto menuDto);
	
	public String consultarMenusHeader(UsuarioDto usuarioDto);
}
