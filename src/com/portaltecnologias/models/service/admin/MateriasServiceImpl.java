package com.portaltecnologias.models.service.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.MateriasDaoInterface;
import com.portaltecnologias.models.dto.common.MateriaDto;
import com.portaltecnologias.models.dto.common.ResponseDto;

@Service
public class MateriasServiceImpl implements MateriasServiceInterface {

	@Autowired
	private MateriasDaoInterface materiasDao;

	@Override
	public String consultarMaterias(String token) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = materiasDao.buscarMaterias();
		if (responseDto.getCodigo() == SUCCESS) {
			System.out.println("Token: " + token);
			@SuppressWarnings("unchecked")
			List<MateriaDto> materias = (List<MateriaDto>) responseDto.getContenido();
			if (token != null && token.length() > 0) {
				materias.add(new MateriaDto("Nueva Materia"));
			}
			materias.add(new MateriaDto("Nueva Materia"));
			responseDto.setContenido(materias);
		}
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String consultarMateriaId(MateriaDto materiaDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = materiasDao.buscarMateriaId(materiaDto.getIdMateria());
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String insertarMateria(MateriaDto materiaDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = materiasDao.insertarMateria(materiaDto);
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String borrarMateria(MateriaDto materiaDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = materiasDao.borrarMateria(materiaDto.getIdMateria());
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String actualizarMateria(MateriaDto materiaDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = materiasDao.actualizarMateria(materiaDto);
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}
}
