package com.portaltecnologias.models.service.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.SeccionesDaoInterface;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.SeccionDto;

@Service
public class SeccionesServiceImpl implements SeccionesServiceInterface {

	@Autowired
	private SeccionesDaoInterface seccionesDao;

	@Override
	public String consultarSecciones(SeccionDto seccionDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = seccionesDao.consultarSecciones(seccionDto.getIdMateria());
		if (responseDto.getCodigo() == SUCCESS) {
			@SuppressWarnings({ "unchecked" })
			List<SeccionDto> secciones = (List<SeccionDto>) responseDto.getContenido();
			secciones.add(new SeccionDto("Nueva Seccion", seccionDto.getIdMateria(), 0, 10));
			responseDto.setContenido(secciones);
		}
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String consultarSeccionId(SeccionDto seccionDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String insertarSeccion(SeccionDto seccionDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = seccionesDao.insertarSeccion(seccionDto);
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String borrarSeccion(SeccionDto seccionDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = seccionesDao.borrarSeccion(seccionDto);
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String actualizarSeccion(SeccionDto seccionDto) {
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = seccionesDao.actualizarSeccion(seccionDto);
		String data = "";
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

}
