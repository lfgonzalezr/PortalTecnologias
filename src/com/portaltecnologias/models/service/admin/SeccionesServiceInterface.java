package com.portaltecnologias.models.service.admin;

import com.portaltecnologias.models.dto.common.SeccionDto;

public interface SeccionesServiceInterface {

	public String consultarSecciones(SeccionDto seccionDto);

	public String consultarSeccionId(SeccionDto seccionDto);

	public String insertarSeccion(SeccionDto seccionDto);

	public String borrarSeccion(SeccionDto seccionDto);

	public String actualizarSeccion(SeccionDto seccionDto);
}
