package com.portaltecnologias.models.service.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.ParrafosDaoInterface;
import com.portaltecnologias.models.dao.admin.TemasDaoInterface;
import com.portaltecnologias.models.dto.common.ResponseDto;
import com.portaltecnologias.models.dto.common.TemasDto;

@Service
public class TemasServiceImpl implements TemasServiceInterface {

	@Autowired
	private TemasDaoInterface temasDao;

	@Autowired
	private ParrafosDaoInterface parrafosDao;

	@Override
	public String consultarTemas(TemasDto temasDto) {
		List<HashMap<String, Object>> temasParrafos = new ArrayList<HashMap<String, Object>>();
		ObjectMapper mapper = new ObjectMapper();
		String data = "";
		ResponseDto responseTemas = temasDao.consultarTemas(temasDto.getIdMateria(), temasDto.getIdSeccion());
		if (responseTemas.getCodigo() == SUCCESS) {
			@SuppressWarnings("unchecked")
			List<TemasDto> temas = (List<TemasDto>) responseTemas.getContenido();
			for (TemasDto temaItem : temas) {
				ResponseDto responseParrafos = parrafosDao.consultarParrafos(temaItem.getIdMateria(),
						temaItem.getIdSeccion(), temaItem.getIdTema());
				HashMap<String, Object> tema = new HashMap<String, Object>();
				tema.put("idMateria", temaItem.getIdMateria());
				tema.put("idSeccion", temaItem.getIdSeccion());
				tema.put("idTema", temaItem.getIdTema());
				tema.put("titulo", temaItem.getTitulo());
				tema.put("subtitulo", temaItem.getSubtitulo());
				tema.put("order", temaItem.getOrder());
				tema.put("parrafos",
						responseParrafos.getContenido() != null ? responseParrafos.getContenido() : new ArrayList<>());
				temasParrafos.add(tema);
			}
		}

		ResponseDto responseDto = new ResponseDto();
		responseDto.setCodigo(0);
		responseDto.setMensage("Busqueda correcta");
		responseDto.setContenido(temasParrafos);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String consultarTemaId(TemasDto temasDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String insertarTema(TemasDto temasDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = temasDao.insertarTema(temasDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String borrarTema(TemasDto temasDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = temasDao.borrarTema(temasDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String actualizarTema(TemasDto temasDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		ResponseDto responseDto = temasDao.actualizarTema(temasDto);
		try {
			data = mapper.writeValueAsString(responseDto);
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

}
