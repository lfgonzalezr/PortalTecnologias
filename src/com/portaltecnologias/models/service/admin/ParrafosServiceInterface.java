package com.portaltecnologias.models.service.admin;

import com.portaltecnologias.models.dto.common.ParrafosDto;

public interface ParrafosServiceInterface {

	public String insertarParrafo(ParrafosDto parrafoDto);

	public String actualizarParrafo(ParrafosDto parrafoDto);

	public String borrarParrafo(ParrafosDto parrafoDto);
}
