package com.portaltecnologias.models.service.admin;

import com.portaltecnologias.models.dto.common.TemasDto;

public interface TemasServiceInterface {

	public String consultarTemas(TemasDto temasDto);

	public String consultarTemaId(TemasDto temasDto);

	public String insertarTema(TemasDto temasDto);

	public String borrarTema(TemasDto temasDto);

	public String actualizarTema(TemasDto temasDto);

}
