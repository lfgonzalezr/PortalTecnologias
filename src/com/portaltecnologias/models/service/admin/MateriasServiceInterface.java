package com.portaltecnologias.models.service.admin;

import com.portaltecnologias.models.dto.common.MateriaDto;

public interface MateriasServiceInterface {

	public String consultarMaterias(String token);

	public String consultarMateriaId(MateriaDto materiaDto);

	public String insertarMateria(MateriaDto materiaDto);

	public String borrarMateria(MateriaDto materiaDto);

	public String actualizarMateria(MateriaDto materiaDto);

}
