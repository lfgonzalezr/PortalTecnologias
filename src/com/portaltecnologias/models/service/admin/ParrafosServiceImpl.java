package com.portaltecnologias.models.service.admin;

import static com.portaltecnologias.constants.ConstantesAdminCommon.JSON_PROCCESSING_EXC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.portaltecnologias.models.dao.admin.ParrafosDaoInterface;
import com.portaltecnologias.models.dto.common.ParrafosDto;

@Service
public class ParrafosServiceImpl implements ParrafosServiceInterface {

	@Autowired
	private ParrafosDaoInterface parrafosDao;

	@Override
	public String insertarParrafo(ParrafosDto parrafoDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			data = mapper.writeValueAsString(parrafosDao.insertarParrafo(parrafoDto));
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String actualizarParrafo(ParrafosDto parrafoDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			data = mapper.writeValueAsString(parrafosDao.actualizarParrafoId(parrafoDto));
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

	@Override
	public String borrarParrafo(ParrafosDto parrafoDto) {
		String data = "";
		ObjectMapper mapper = new ObjectMapper();
		try {
			data = mapper.writeValueAsString(parrafosDao.borrarParrafo(parrafoDto));
		} catch (JsonProcessingException e) {
			data = JSON_PROCCESSING_EXC;
		}
		return data;
	}

}
