package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.ParrafosDto;

public class ParrafosDtoMapper implements RowMapper<List<ParrafosDto>> {

	@Override
	public List<ParrafosDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		List<ParrafosDto> parrafos = new ArrayList<ParrafosDto>();
		if (resultSet != null) {
			while (resultSet.next()) {
				ParrafosDto parrafo = new ParrafosDto();
				parrafo.setIdMateria(resultSet.getInt("FI_ID_MATERIA"));
				parrafo.setIdSeccion(resultSet.getInt("FI_ID_SECCION"));
				parrafo.setIdTema(resultSet.getInt("FI_ID_TEMA"));
				parrafo.setIdParrafo(resultSet.getInt("FI_ID_PARRAFO"));
				parrafo.setTitulo(resultSet.getString("FC_TITULO"));
				parrafo.setSubtitulo(resultSet.getString("FC_SUBTITULO"));
				parrafo.setTextoParrafo(resultSet.getString("FC_TEXTO_PARRAFO"));
				parrafo.setOrder(resultSet.getInt("FI_ORDER"));
				parrafos.add(parrafo);
			}
		}
		return parrafos;
	}

}
