package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.TemasDto;

public class TemasDtoMapper implements RowMapper<List<TemasDto>> {

	@Override
	public List<TemasDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		List<TemasDto> temas = new ArrayList<TemasDto>();
		if (resultSet != null) {
			while (resultSet.next()) {
				TemasDto tema = new TemasDto();
				tema.setIdTema(resultSet.getInt("FI_ID_TEMA"));
				tema.setIdSeccion(resultSet.getInt("FI_ID_SECCION"));
				tema.setIdMateria(resultSet.getInt("FI_ID_MATERIA"));
				tema.setTitulo(resultSet.getString("FC_TITULO"));
				tema.setSubtitulo(resultSet.getString("FC_SUBTITULO"));
				tema.setOrder(resultSet.getInt("FI_ORDER"));
				temas.add(tema);
			}
		}
		return temas;
	}

}
