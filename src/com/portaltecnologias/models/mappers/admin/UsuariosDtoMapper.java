package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.UsuarioDto;

public class UsuariosDtoMapper implements RowMapper<ArrayList<UsuarioDto>> {

	@Override
	public ArrayList<UsuarioDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		ArrayList<UsuarioDto> usuarios = new ArrayList<>();
		if (resultSet != null) {
			while (resultSet.next()) {
				UsuarioDto usuarioDto = new UsuarioDto();
				usuarioDto.setCorreo(resultSet.getString("FC_CORREO"));
				usuarioDto.setIdUsuario(resultSet.getInt("FI_ID_USUARIO"));
				usuarioDto.setNombreUsuario(resultSet.getString("FC_NOMBRE"));
				usuarioDto.setPassword(resultSet.getString("FC_PASSWORD"));
				usuarioDto.setStatus(resultSet.getInt("FI_STATUS"));
				usuarios.add(usuarioDto);
			}
		}
		return usuarios;
	}

}
