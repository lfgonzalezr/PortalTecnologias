package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.RolDto;

public class RolDtoMapper implements RowMapper<ArrayList<RolDto>> {

	@Override
	public ArrayList<RolDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		ArrayList<RolDto> lista = new ArrayList<>();
		if (resultSet != null) {
			while (resultSet.next()) {
				RolDto rolDto = new RolDto();
				rolDto.setDescripcion(resultSet.getString("FC_DESCRIPCION"));
				rolDto.setIdRol(resultSet.getInt("FI_ID_ROL"));
				rolDto.setNombre(resultSet.getString("FC_NOMBRE"));
				rolDto.setStatus(resultSet.getInt("FI_STATUS"));
				lista.add(rolDto);
			}
		}
		return lista;
	}
}
