package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.MenuDto;

public class MenusDtoMapper implements RowMapper<ArrayList<MenuDto>> {

	@Override
	public ArrayList<MenuDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		ArrayList<MenuDto> lista = new ArrayList<>();
		if (resultSet != null) {
			while (resultSet.next()) {
				MenuDto menu = new MenuDto();
				menu.setIdMenu(resultSet.getInt("FI_ID_MENU"));
				menu.setNombre(resultSet.getString("FC_NOMBRE"));
				menu.setDescripcion(resultSet.getString("FC_DESCRIPCION"));
				menu.setStatus(resultSet.getInt("FI_STATUS"));
				menu.setIcono(resultSet.getString("FC_ICONO"));
				menu.setTipoElemento(resultSet.getInt("FI_TIPO_ELEMENTO"));
				menu.setUri(resultSet.getString("FC_URL"));
				menu.setOrden(resultSet.getInt("FI_ORDEN"));
				lista.add(menu);
			}
		}
		return lista;
	}
}
