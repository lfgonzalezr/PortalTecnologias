package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.SeccionDto;

public class SeccionesDtoMapper implements RowMapper<List<SeccionDto>> {

	@Override
	public List<SeccionDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		List<SeccionDto> secciones = new ArrayList<SeccionDto>();
		if (resultSet != null) {
			while (resultSet.next()) {
				SeccionDto seccion = new SeccionDto();
				seccion.setIdSeccion(resultSet.getInt("FI_ID_SECCION"));
				seccion.setIdMateria(resultSet.getInt("FI_ID_MATERIA"));
				seccion.setTitulo(resultSet.getString("FC_TITULO"));
				seccion.setSubtitulo(resultSet.getString("FC_SUBTITULO"));
				seccion.setOrder(resultSet.getInt("FI_ORDER"));
				secciones.add(seccion);
			}
		}
		return secciones;
	}

}
