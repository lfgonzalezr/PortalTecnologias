package com.portaltecnologias.models.mappers.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.portaltecnologias.models.dto.common.MateriaDto;

public class MateriasDtoMapper implements RowMapper<List<MateriaDto>> {

	@Override
	public List<MateriaDto> mapRow(ResultSet resultSet, int arg1) throws SQLException {
		List<MateriaDto> materias = new ArrayList<MateriaDto>();
		if (resultSet != null) {
			while (resultSet.next()) {
				MateriaDto materia = new MateriaDto();
				materia.setTitulo(resultSet.getString("FC_TITULO"));
				materia.setIdMateria(resultSet.getInt("FI_ID_MATERIA"));
				materia.setDescripcion(resultSet.getString("FC_DESCRIPCION"));
				materia.setUrl(resultSet.getString("FC_URL"));
				materia.setOrder(resultSet.getInt("FI_ORDER"));
				materias.add(materia);
			}
		}
		return materias;
	}

}
