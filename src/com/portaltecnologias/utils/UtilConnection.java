package com.portaltecnologias.utils;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class UtilConnection {

	private static DataSource dataSource;

	private UtilConnection() {
		// Do nothing
	}

	public static final Connection getConnection() throws SQLException {
		if (dataSource == null) {
			try {
				Context context = new InitialContext();
				Context env = (Context) context.lookup("java:comp/env");
				dataSource = (DataSource) env.lookup("jdbc/tec");
			} catch (NamingException e) {
				System.out.println("Error en getConnection :" + e.getMessage());
			}
		}
		return dataSource.getConnection();
	}
}
