/*
 * Clase en de-uso, se realizo el cambio en la forma de conexion a la BD.
 * Se integro el uso de un jndi de tomcat
 * */
package com.portaltecnologias.utils;

import static com.portaltecnologias.constants.ConstantesAdminCommon.ORACLE_DRIVER;
import static com.portaltecnologias.constants.ConstantesAdminCommon.PSSWRD_CONECTION;
import static com.portaltecnologias.constants.ConstantesAdminCommon.URL_CONECTION;
import static com.portaltecnologias.constants.ConstantesAdminCommon.USER_CONECTION;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class UtilsConnection {

	public static final Connection getConection() throws ClassNotFoundException, SQLException{
		Class.forName(ORACLE_DRIVER);
		Connection connection = null;
		Properties properties = new Properties();
		properties.put("user", USER_CONECTION);
		properties.put("password", PSSWRD_CONECTION);
		connection = DriverManager.getConnection(URL_CONECTION, properties);
		return connection;
	}
}