/**
 * Clase donde se encuentran los id de materias y otros valores estaticos
 */
package com.portaltecnologias.constants;

/**
 * @author lfgonzalezr
 *
 */
public class ConstantesAdminCommon {
    // Constantes para consultas finalizadas correctamente 
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
    // Constantes para obtener datos de BD
    public static final String CODE = "CODE_OUT";
    public static final String MSGE = "MSGE_OUT";
    public static final String DATA = "DATA_OUT";
    // Constantes para excepciones de sistema
    public static final int ERR_DAO = -100;
    public static final int ERR_SERVICE = -10;
    // Err al procesar la respuesta 
    public static final String JSON_PROCCESSING_EXC = "{\"codE\":-1,\"msgE\":\"JsonProcessingException\",\"jsonResultado\":\"" + null + "\"}";
    // Err en controller, metodo no encontrado 
    public static final String JSON_CONTROLLER_EXC = "{\"codE\":-1,\"msgE\":\"Servicio no existente\",\"jsonResultado\":\"" + null + "\"}";
    // Constantes de conexion a BD
    public static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String URL_CONECTION = "jdbc:oracle:thin:@localhost:1521:XE";
    public static final String USER_CONECTION = "TECNOLOGICAS";
    public static final String PSSWRD_CONECTION = "1408";
}
