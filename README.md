Este proyecto trata de generar un blog en el cual se pueda publicar 
contenido de diferentes temas. 

Tecnologias utilizadas en esta página:

*   ReactJs
*   BootStrap
*   Spring Core
*   Oracle PL/SQL

Para el desarrollo de este proyecto se decidio no utilizar gestor 
de dependencias, con la finalidad de integrar de forma manual las librerias 
necesarias, por lo cual las librerias son descargadas como jar de 
sus páginas oficiales.

ReactJS:
Tutorial pata iniciar el proyecto
https://reactjs.org/tutorial/tutorial.html

Bootstrap v3.3.7:
http://blog.getbootstrap.com/2016/07/25/bootstrap-3-3-7-released/

Spring 4.2
https://repo.spring.io/release/org/springframework/spring/4.2.9.RELEASE/

OJDBC 11g
https://www.oracle.com/technetwork/apps-tech/jdbc-112010-090769.html

Apache Tomcat v8.0.53
https://tomcat.apache.org/download-80.cgi