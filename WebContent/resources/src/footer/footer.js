import React, { Component } from 'react';

export default class Footer extends Component{

  render() {
    return(
      <footer>
        <nav className="container-fluid pt-box-out-box">
          <div className="row">
            <div className="col-sm-4">
              <div className="thumbnail">
                <div className="caption">
                  <h3><i className="icon-react" /><i className="icon-redux" /><i className="icon-css3" /><i className="icon-html5" /><i className="icon-java" /></h3>
                  <p>Created and Design by</p>
                  <p>@lfgonzalezr</p>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="thumbnail">
                <div className="caption">
                  <h3>Contacto</h3>
                  <p><a><i className="icon-linkedin" /></a><a><i className="icon-twitter" /></a><a><i className="icon-facebook" /></a></p>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="thumbnail">
                <div className="caption">
                  <h3>Enlaces</h3>
                  <p>Acerca de</p>
                  <p>Contacto</p>
                </div>
              </div>
            </div>
          </div>
        </nav>
      </footer>);
  }
}
