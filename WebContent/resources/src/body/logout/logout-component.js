import React, { Component } from 'react';
import { post } from '../../utils/utils.js';

export default class LogoutComponent extends Component{

  constructor(props){
    super(props);
  }

  render() {
    return(
      <div className="container pt-box-out-box pt-space-top">
        <div className="jumbotron pt-text-center">
          <h2>logOut</h2>
        </div>
      </div>
    );
  }
}
