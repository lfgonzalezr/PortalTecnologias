import React, { Component } from 'react';
import { ValidateForm } from '../../../utils/utils.js';
import { post } from '../../../utils/utils.js';
import Input from '../../form/input.js';

export default class CardNuevaSeccion extends Component {
  constructor(props){
    super(props);
    this.state = {
      statusFormClass: "form-lbl-error",
      statusFormLabel: "",
      formulario: ''
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    var validacion = ValidateForm("formulario-nueva-seccion");
    if(!validacion.complete){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout(()=>{
        this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"});
      }, 4200);
      return;
    }
    var url = this.props.title === 'Editar' ? '/admin/seccion/actualizar' : '/admin/seccion/crear' ;
    post(url, validacion.json, (data)=> {
      if(data.codigo === 0){
        this.props.close();
      } else {
        console.log(data);
      }
    });
  }

  eliminar = (data, e) => {
    e.preventDefault();
    var validacion = ValidateForm("formulario-nueva-seccion");
    if(!validacion.complete){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout(()=>{
        this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"});
      }, 4200);
      return;
    }
    var url = '/admin/seccion/borrar';
    post(url, validacion.json, (data)=> {
      if(data.codigo === 0){
        this.props.close();
      } else {
        console.log(data);
      }
    });
  }

  cancelar = ( e ) => {
    e.preventDefault();
    this.props.close();
  }

  render() {
    const { data, title } = this.props;
    return(
      <div className="container pt-width-modal">
        <form onSubmit={this.onSubmit.bind(this)} id="formulario-nueva-seccion" autoComplete="off" noValidate>
          <div className="form-group">
            <h2>{ title } Seccion</h2>
          </div>
          <div className="form-group pt-hidden">
            <label>Id Materia</label>
            <Input required="true" typeVal="numeric" placeholder="id materia" name="idMateria" value={data.idMateria} readonly="true" />
          </div>
          <div className="form-group pt-hidden">
            <label>Id Seccion</label>
            <Input required="false" typeVal="numeric" placeholder="Id Seccion" name="idSeccion" value={data.idSeccion} readonly="true" />
          </div>
          <div className="form-group">
            <label>Titulo</label>
            <Input required="true" typeVal="alpha" placeholder="titulo" name="titulo" value={data.titulo} readonly="false" />
          </div>
          <div className="form-group">
            <label>Subtitulo</label>
            <Input required="true" typeVal="alpha" placeholder="subtitulo" name="subtitulo" value={data.subtitulo} readonly="false" />
          </div>
          <div className="form-group">
            <label>Orden</label>
            <Input required="true" typeVal="numeric" placeholder="orden" name="order" value={data.order} readonly="false" />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-success pt-btn-space">Enviar</button>
            <button onClick={ this.cancelar.bind(this) } className="btn btn-secondary pt-btn-space"> Cancelar </button>
            { title === "Editar" ? <button className="btn btn-danger float-right" onClick={this.eliminar.bind(this, data)}> Eliminar </button> : "" }
          </div>
        </form>
        <div className={ this.state.statusFormClass }>
          <label>{ this.state.statusFormLabel }</label>
        </div>
      </div>);
    }
  }
