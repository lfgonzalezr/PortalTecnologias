import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import CardNuevoParrafo from '../card-parrafo/card-nuevo-parrafo.js';
import CardNuevoTema from './card-nuevo-tema.js';

export default class CardTema extends Component {

  constructor(props){
    super(props);
    this.state = {
      open: false,
      formulario: null
    };
  }

  editarParrafo = (parrafo, e) => {
    e.preventDefault();
    this.setState({formulario : <CardNuevoParrafo data={ parrafo } title="Editar" edit="true" close={ this.onCloseModal } />, open: true});
  }

  nuevoParrafo = ( tema, e ) => {
    e.preventDefault();
    var parrafo = {
      idMateria: tema.idMateria,
      idSeccion: tema.idSeccion,
      idTema: tema.idTema
    };
    this.setState({formulario : <CardNuevoParrafo data={ parrafo } title="Nuevo" edit="false" close={ this.onCloseModal } />, open: true});
  }

  editarTema = (tema, e) => {
    e.preventDefault();
    this.setState({formulario : <CardNuevoTema data={ tema } close={ this.onCloseModal } title="Editar" edit="true" />, open: true});
  };

  onCloseModal = (idMateria, idSeccion, actualizar) => {
    this.setState({ open: false });
    this.props.close(idMateria, idSeccion, actualizar);
  }

  render(){
    const { open, formulario } = this.state;
    const { data } = this.props;
    return(
      <div>{
        data.map( (tema, indexTema) => {
          return (
            <div key={indexTema}>
              <div className="page-header pt-margin-top-title">
                <h3> { tema.titulo } </h3>
                <h3><small> { tema.subtitulo } </small></h3>
              </div>{
                tema.parrafos.map( (parrafo, indexParrafo) => {
                  return(
                    <article key={indexParrafo}>
                      <span><h3> { parrafo.titulo } </h3></span>
                      <span><h4> { parrafo.subtitulo } </h4></span>
                      <p> { parrafo.textoParrafo } </p>
                      <ul className="pager">
                        <li className="next" onClick={this.editarParrafo.bind(this, parrafo)}><a href="/">Editar Parrafo</a></li>
                      </ul>
                    </article>
                  );
                })
              }<ul className="pager">
                <li className="previous" onClick={this.editarTema.bind(this, tema)}><a href="/">Editar Tema</a></li>
                <li className="next" onClick={this.nuevoParrafo.bind(this, tema)}><a href="/">Nuevo Parrafo</a></li>
              </ul>
            </div>);
        })
      }<Modal open={open} onClose={this.onCloseModal} center>
          { formulario }
        </Modal>
      </div>
    );
  }
}
