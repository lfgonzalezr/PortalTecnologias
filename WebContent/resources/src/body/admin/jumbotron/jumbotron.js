import React, { Component } from 'react';

export default class Jumbotron extends Component {

  render(){
    return(
      <div className="jumbotron">
        <h2>{this.props.data.title}</h2>
        <h2>
          <small>{this.props.data.description}</small>
        </h2>
      </div>
    );
  }
}
