import React, { Component } from 'react';
import { post } from '../../../utils/utils.js';
import Input from '../../form/input.js';
import TextArea from '../../form/textarea.js';
import { ValidateForm } from '../../../utils/utils.js';

export default class CardNuevoParrafo extends Component {
  constructor(props){
    super(props);
    this.state = {
      statusFormClass: "form-lbl-error",
      statusFormLabel: ""
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    var validacion = ValidateForm("formulario-nuevo-parrafo");
    if(!validacion.complete){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout(()=>{
        this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"});
      }, 4200);
      return;
    }
    var url = this.props.title === 'Editar' ? '/admin/parrafo/actualizar' : '/admin/parrafo/crear' ;
    post(url, validacion.json, (data)=> {
      if(data.codigo === 0){
        this.props.close(validacion.json.idMateria, validacion.json.idSeccion, true);
      } else {
        console.log(data);
      }
    });
  }

  eliminar = (data, e) => {
    e.preventDefault();
    var validacion = ValidateForm("formulario-nuevo-parrafo");
    if(!validacion.complete){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout(()=>{
        this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"});
      }, 4200);
      return;
    }
    var url = '/admin/parrafo/borrar';
    post(url, validacion.json, (data)=> {
      if(data.codigo === 0){
        this.props.close(validacion.json.idMateria, validacion.json.idSeccion, true);
      } else {
        console.log(data);
      }
    });
  }

  cancelar = (e) => {
    this.props.close(0, 0, false);
  }

  render(){
    const { data, title } = this.props;
    return(
      <div className="container pt-width-modal">
        <form onSubmit={this.onSubmit.bind(this)} id="formulario-nuevo-parrafo" autoComplete="off" noValidate>
          <div className="form-group">
            <h2>{ title } Parrafo</h2>
          </div>
          <div className="form-group pt-hidden">
            <label>Id Materia</label>
            <Input required="true" typeVal="numeric" placeholder="id materia" name="idMateria" value={data.idMateria} readonly="true" />
          </div>
          <div className="form-group pt-hidden">
            <label>Id Seccion</label>
            <Input required="true" typeVal="numeric" placeholder="id seccion" name="idSeccion" value={data.idSeccion} readonly="true" />
          </div>
          <div className="form-group pt-hidden">
            <label>Id Tema</label>
            <Input required="true" typeVal="numeric" placeholder="id tema" name="idTema" value={data.idTema} readonly="true" />
          </div>
          <div className="form-group pt-hidden">
            <label>Id Parrafo</label>
            <Input typeVal="numeric" placeholder="id parrafo" name="idParrafo" value={data.idParrafo} readonly="true" />
          </div>
          <div className="form-group">
            <label>Titulo</label>
            <Input typeVal="alpha" placeholder="titulo (opcional)" name="titulo" value={data.titulo} />
          </div>
          <div className="form-group">
            <label>Subtitulo</label>
            <Input typeVal="alpha" placeholder="subtitulo (opcional)" name="subtitulo" value={data.subtitulo} />
          </div>
          <div className="form-group">
            <label>Orden</label>
            <Input required="true" typeVal="numeric" placeholder="orden" name="order" value={data.order} />
          </div>
          <div className="form-group">
            <label>Contenido</label>
            <TextArea name="textoParrafo" placeholder="Contenido del parrafo" required="true" className="form-control" value={data.textoParrafo} />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-success pt-btn-space">Enviar</button>
            <button onClick={this.cancelar.bind(this)} className="btn btn-secondary pt-btn-space">Cancelar</button>
            { title === "Editar" ? <button className="btn btn-danger float-right" onClick={this.eliminar.bind(this, this.props.data)}> Eliminar </button> : "" }
          </div>
        </form>
        <div className={ this.state.statusFormClass }>
          <label>{ this.state.statusFormLabel }</label>
        </div>
      </div>);
  }
}
