import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import CardTema from '../card-tema/card-tema.js';
import MenuLateral from '../../menu-lateral/menu-lateral.js';
import CardNuevoTema from '../card-tema/card-nuevo-tema.js';
import CardNuevaSeccion from '../card-seccion/card-nueva-seccion.js';
import { post } from '../../../utils/utils.js';

export default class AdminPrincipal extends Component {
  constructor(props){
    super(props);
    this.state = {
      modalTema: false,
      modalSeccion: false,
      secciones: [],
      temas: [],
      nuevaSeccion: {}
    };
  }

  componentDidMount(){
    this.onCloseModalSecciones();
  }

  selectSection = (idMateria, itemSeccion, action) => {
    switch (action) {
      case 'new':
        var item = { idMateria: itemSeccion.idMateria };
        this.setState({modalSeccion: true, seccion: item, title:'Nuevo'});
        break;
      case 'edit':
        this.setState({modalSeccion: true, seccion: itemSeccion, title:'Editar'});
        break;
      default:
        var urlTemas = '/admin/tema/consultar';
        var jsonSend = {idMateria: idMateria, idSeccion: itemSeccion};
        this.setState({ seccion: jsonSend});
        post(urlTemas, jsonSend , ( data ) => {
          if(data.codigo === 0){
            this.setState({temas: data.contenido});
          }
        });
    }
  }
  regresar = (e) => {
    e.preventDefault();
    this.props.regresar(0);
  }

  nuevoTema = (e) => {
    e.preventDefault();
    this.setState({modalTema: true, nuevoTema: this.state.seccion });
  }

  onCloseModal = () => {
    this.setState({modalTema: false, modalSeccion: false});
  }

  onCloseModalSecciones = () => {
    this.setState({modalTema: false, modalSeccion: false});
    var urlSecciones = '/admin/seccion/consultar';
    post(urlSecciones, {idMateria: this.props.idMateria}, ( data ) =>{
      if(data.codigo === 0){
        this.setState({secciones: data.contenido});
      } else {
        console.log(data);
      }
      this.selectSection(this.props.idMateria, 1, '');
    });
  }

  onCloseModalTemas = (idMateria, idSeccion, actualizar) => {
    this.setState({modalTema: false, modalSeccion: false});
    if( actualizar ) {
      this.selectSection(idMateria, idSeccion, '');
    }
  }

  render(){
    const { modalTema, modalSeccion, seccion, secciones, title, nuevoTema, temas } = this.state;
    return(
      <div className="pt-box-out-box pt-space-top-50">
        <MenuLateral onClickElement={ this.selectSection } regresar={ this.regresar } data={ secciones } />
        <div className="col-sm-10">
          <CardTema data={ temas } close={this.onCloseModalTemas} />
          <ul className="pager">
            { secciones.length > 1 ? <li><a href="/"  onClick={ this.nuevoTema.bind() }>Nuevo Tema</a></li> : "" }
          </ul>
        </div>
        <Modal open={ modalTema } onClose={this.onCloseModal} center>
          <CardNuevoTema data={ nuevoTema } title="Nuevo" edit="false" close={this.onCloseModalTemas} />
        </Modal>
        <Modal open={ modalSeccion } onClose={ this.onCloseModal } center>
          <CardNuevaSeccion data={ seccion } title={ title } close={ this.onCloseModalSecciones } />
        </Modal>
      </div>
    );
  }
}
