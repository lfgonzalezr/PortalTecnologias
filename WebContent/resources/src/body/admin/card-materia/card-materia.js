import React, { Component } from 'react';

export default class CardMateria extends Component {

  onClickHeader = ( item , e ) => {
    this.props.clickOnCard(item, 'editar');
  }

  onClickBody = ( item, e ) => {
    if(item.idMateria === 0){
      this.props.clickOnCard(item, 'nuevo');
    } else {
      this.props.clickOnCard(item, 'seleccionar');
    }
  }

  render(){
    const { data } = this.props;
    const editable = ( data.idMateria > 0 && this.props.editable );
    return(
      <div className="col-sm-4">
        <div className="pt-box-item">
          <div className="panel panel-primary">
            <div className="panel-heading pt-upper-case">
              { data.titulo }
              { editable ? <i className="glyphicon glyphicon-option-vertical float-right" onClick={ this.onClickHeader.bind( this, data ) }></i> : "" }
            </div>
            <div className="panel-body pt-100px" onClick={ this.onClickBody.bind( this, data ) }>  { data.descripcion } </div>
          </div>
        </div>
      </div>
    );
  }
}
