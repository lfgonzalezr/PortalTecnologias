import React, { Component } from 'react';
import Input from '../../form/input.js';
import { ValidateForm } from '../../../utils/utils.js';
import { post } from '../../../utils/utils.js';

export default class CardNuevaMateria extends Component {
  constructor(props){
    super(props);
    this.state = {
      statusFormClass: "form-lbl-error",
      statusFormLabel: ""
    }
  }

  onSubmit = ( e ) => {
    e.preventDefault();
    var validacion = ValidateForm("formulario-nueva-materia");
    if( !validacion.complete ){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout(()=>{
        this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"});
      }, 4200);
      return;
    }
    var url = this.props.title === 'Editar' ? '/admin/materia/actualizar' : '/admin/materia/crear' ;
    post(url, validacion.json, (data)=> {
      if(data.codigo === 0){
        this.props.onClose();
      }
    });
  }

  eliminar = (data, e) => {
    e.preventDefault();
    var url = '/admin/materia/borrar';
    post(url, data, (data) => {
      if(data.codigo === 0){
        this.props.onClose();
      }
    });
  }

  cancelar = ( e ) => {
    e.preventDefault();
    this.props.onClose();
  }

  render(){
    const { statusFormClass, statusFormLabel } = this.state;
    const { data, title } = this.props;
    return(
      <div className="container pt-width-modal">
        <form onSubmit={ this.onSubmit.bind(this) } id="formulario-nueva-materia" autoComplete="off" noValidate>
          <div className="form-group">
            <h2> { title } Materia </h2>
          </div>
          <div className="form-group">
            <label>Id Materia</label>
            <Input required="true" typeVal="numeric" placeholder="id materia" name="idMateria" value={ data.idMateria } readonly={ title === 'Editar' ? "true" : "false"} />
          </div>
          <div className="form-group">
            <label>Titulo</label>
            <Input required="true" typeVal="alpha" placeholder="titulo" name="titulo" value={ data.titulo } readonly="false" />
          </div>
          <div className="form-group">
            <label>Descripción</label>
            <Input typeVal="alpha" placeholder="descripcion" name="descripcion" value={ data.descripcion } readonly="false" />
          </div>
          <div className="form-group">
            <label>Url</label>
            <Input required="true" typeVal="alpha" placeholder="url" name="url" value={ data.url } readonly="false" />
          </div>
          <div className="form-group">
            <label>Orden</label>
            <Input required="true" typeVal="numeric" placeholder="orden" name="order" value={ data.order } readonly="false" />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-success pt-btn-space"> { title === 'Editar' ? "Actualizar" : "Guardar"} </button>
            <button onClick={ this.cancelar.bind(this) } className="btn btn-secondary pt-btn-space"> Cancelar </button>
            { title === 'Editar' ? <button onClick={ this.eliminar.bind(this, data) } className="btn btn-danger float-right"> Eliminar </button> : "" }
          </div>
        </form>
        <div className={ statusFormClass }>
          <label>{ statusFormLabel }</label>
        </div>
      </div>);
  }
}
