import React, { Component } from 'react';
import Home from './admin-home/home.js';
import AdminPrincipal from './admin-principal/admin-principal.js';
import { post } from '../../utils/utils.js';

export default class AdminHome extends Component {

  constructor(props){
    super(props);
    this.state = ({
      view: 'Home',
      dataMaterias: []
    });
  }

  componentDidMount() {
    this.queryMaterias();
  }

  queryMaterias = () => {
    var url = '/admin/materia/consultar';
    post(url, {}, ( data ) => {
      if(data.codigo === 0){
        this.setState({ dataMaterias: data.contenido });
      } else {
        console.log(data);
      }
    });
  }

  onSelect = ( item ) => {
    if(item !== 0){
      this.setState({
        view: 'AdminPrincipal',
        idMateria: item
      });
    } else {
      this.setState({
        view: 'Home'
      });
    }
  }

  render(){
    const { dataMaterias, view, idMateria } = this.state;
    switch (view) {
      case 'Home':
          return(<Home data={ dataMaterias } onSelect={ this.onSelect } queryMaterias={ this.queryMaterias } />);
      case 'AdminPrincipal':
          return(<AdminPrincipal idMateria={ idMateria } regresar={ this.onSelect }/>);
      default:
    }
  }
}
