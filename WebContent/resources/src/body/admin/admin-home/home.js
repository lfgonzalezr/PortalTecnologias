import React, { Component } from 'react';
import Modal from 'react-responsive-modal';
import Jumbotron from '../jumbotron/jumbotron.js';
import CardMateria from '../card-materia/card-materia.js';
import CardNuevaMateria from '../card-materia/card-nueva-materia.js';
import { textAdminHome } from './data-txt.js';
import store from '../../../utils/store';

export default class Home extends Component {

  constructor(props){
    super(props);
    this.state = ({
      modal: false,
      textAdminHome: textAdminHome,
      materia: {},
      title: '',
      editable: store.getState().dataLogin.typeLogin
    });
  }

  clickOnCard = ( item, target ) => {
    switch(target){
      case 'nuevo':
        this.setState({ modal: true, materia: {}, title:'Nuevo' });
        break;
      case 'editar':
        this.setState({ modal: true, materia :item, title: 'Editar' });
        break;
      default:
        this.props.onSelect(item.idMateria);
    }
  }

  onCloseModal = () => {
    this.setState({ modal: false });
    this.props.queryMaterias();
  }

  render(){
    const { data } = this.props;
    const { textAdminHome, modal, materia, title, editable } = this.state;
    return(
      <div className="container pt-box-out-box pt-space-top">
        <Jumbotron data={ textAdminHome } />
        <div className="row">{
          data.map( (materia, index) => {
            return (<CardMateria
              key={ materia.idMateria }
              data={ materia }
              clickOnCard={ this.clickOnCard }
              editable={ editable }
              />);
          })
        }</div>
        <Modal open={ modal } onClose={ this.onCloseModal } center>
          <CardNuevaMateria onClose={ this.onCloseModal } title={ title } data={ materia }/>
        </Modal>
      </div>
    );
  }
}
