import React, { Component } from 'react';

export default class TextArea extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: props.value === undefined || props.value ===  null ? "" : props.value,
      required: props.required === 'true' ? true : false,
      readonly: props.readonly === 'true' ? true : false,
      classNameError: 'form-lbl-error'
    };
  }

  onChangeEvt = (e) => {
    this.setState({value: e.target.value});
  }

  focusOutEvt = (e) => {
    if (this.state.required && (e.target.value.length < 1)){
        this.setState({
          classNameError: 'form-lbl-error active'
        });
    }
  }

  keyPressEvt = (e) => {
    this.setState({classNameError: 'form-lbl-error'});
    var idType = ['alpha', 'numeric'].indexOf(this.state.typeVal);
    switch (idType) {
      case 0:
        if(e.keyCode < 64 || e.keyCode > 91){
          if([8, 9, 16, 32, 36, 37, 38, 39, 40, 192].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      case 1:
        if(e.keyCode < 48 || ( e.keyCode > 57 && e.keyCode < 96 ) || e.keyCode > 105){
          if([8, 9, 16, 36, 37, 38, 39, 40].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      default:
    }
  }

  render(){
    if(this.props.type === 'checkbox'){
      return(
        <div className="form-check">
          <input
            className="form-check-input"
            type={this.props.type}
            />
        </div>);
    } else {
      return(
        <div className="form-group">
          <textarea
            name={this.props.name}
            className="form-control pt-resize-none"
            value={this.state.value}
            placeholder={this.props.placeholder}
            onBlur={this.focusOutEvt.bind(this)}
            onKeyDown={this.keyPressEvt.bind(this)}
            readOnly={this.state.readonly}
            required={this.state.required}
            onChange={this.onChangeEvt.bind(this)}
            rows="10"
            cols="50"
            ></textarea>
            <div className={this.state.classNameError}>
              <label>{this.props.msgE !== undefined ? this.props.msgE : 'Campo requerido'}</label>
            </div>
        </div>
      );
    }
  }
}
