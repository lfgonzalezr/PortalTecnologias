import React, { Component } from 'react';

export default class Input extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: props.value === undefined || props.value ===  null ? "" : props.value,
      required: props.required === 'true' ? true : false,
      readonly: props.readonly === 'true' ? true : false,
      typeVal: props.typeVal !== undefined ? props.typeVal: '',
      pattern: props.pattern !== undefined ? props.pattern : '',
      classNameError: 'form-lbl-error'
    };
  }

  onChangeEvt = (e) => {
    this.setState({value: e.target.value});
  }

  focusOutEvt = (e) => {
    if (this.state.required && (e.target.value.length < 1)){
        this.setState({
          classNameError: 'form-lbl-error active'
        });
    }
  }

  keyPressEvt = (e) => {
    this.setState({classNameError: 'form-lbl-error'});
    var idType = ['alpha', 'numeric', 'alpha-numeric', 'alpha-restrict', 'email'].indexOf(this.state.typeVal);
    switch (idType) {
      case 0:// alpha
        if(e.keyCode < 64 || e.keyCode > 91){
          if([8, 9, 16, 32, 36, 37, 38, 39, 40, 192].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      case 1:// numeric
        if(e.keyCode < 48 || ( e.keyCode > 57 && e.keyCode < 96 ) || e.keyCode > 105){
          if([8, 9, 16, 36, 37, 38, 39, 40].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      case 2:// alpha-numeric
        if(e.keyCode < 48 || ( e.keyCode > 57  && e.keyCode < 65 ) || ( e.keyCode > 90 && e.keyCode < 96 ) || e.keyCode > 105 || e.shiftKey){
          if([8, 9, 16, 35, 36, 37, 38, 39, 40, 46, 49, 163, 190].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      case 3:// alpha-restrict
        if(e.keyCode < 48 || ( e.keyCode > 57  && e.keyCode < 65 ) || ( e.keyCode > 90 && e.keyCode < 96 ) || e.keyCode > 105){
          if([8, 9, 16, 32, 36, 37, 38, 39, 40, 192, 190, 189].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      case 4:// email
        if(e.keyCode < 48 || ( e.keyCode > 57  && e.keyCode < 65 ) || ( e.keyCode > 90 && e.keyCode < 96 ) || e.keyCode > 105 || e.shiftKey){
          if([8, 9, 16, 35, 36, 37, 38, 39, 40, 46, 49, 163, 189, 190].indexOf(e.keyCode) === -1){
            e.preventDefault();
          }
        }
        break;
      default:
    }
  }

  render(){
    if(this.props.type === 'checkbox'){
      return(
        <div className="form-check">
          <input
            className="form-check-input"
            type={this.props.type}
            />
        </div>);
    } else {
      return(
        <div className="form-group">
          <input
            name={this.props.name}
            className="form-control"
            value={this.state.value}
            maxLength={this.props.length !== undefined ? this.props.length : 100 }
            type={this.props.type !== undefined ? this.props.type : 'text'}
            placeholder={this.props.placeholder}
            onBlur={this.focusOutEvt.bind(this)}
            onKeyDown={this.keyPressEvt.bind(this)}
            readOnly={this.state.readonly}
            required={this.state.required}
            onChange={this.onChangeEvt.bind(this)}
            />
            <div className={this.state.classNameError}>
              <label>{this.props.msgE !== undefined ? this.props.msgE : 'Campo requerido'}</label>
            </div>
        </div>
      );
    }
  }
}
