import React, { Component } from 'react';

export default class Home extends Component{

  render(){
    return(
      <div className="container pt-box-out-box pt-space-top">
        <div className="row col-sm-12">
          <div className="jumbotron">
            <h2>Tecnologias</h2>
            <h2>
              <small>Pagina con informacion, referencias a tutoriales y anotaciones de algunas de las tecnologias disponibles.</small>
            </h2>
          </div>
        </div>
      </div>
    );
  }
}
