import React, { Component } from 'react';
import Input from '../form/input.js';
import './login.css';

export default class FormularioLogin extends Component {

  render(){
    return(
      <form onSubmit={ this.props.onSubmit } id="formulario-login" autoComplete="off" noValidate>
        <div className="form-group">
          <h2>Login</h2>
        </div>
        <div className="form-group">
          <label>Correo</label>
          <Input required="true" typeVal="email" placeholder="Correo" name="correo" readonly="false" />
        </div>
        <div className="form-group">
          <label>Password</label>
          <Input required="true" typeVal="alpha-numeric" placeholder="Password" name="password" type="password" readonly="false" />
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-success pt-btn-space"> Entrar </button>
          <a className="btn btn-secondary pt-btn-space" href="/" onClick={ this.props.cancelar } > Cancelar </a>
          <a className="btn btn-secondary float-right" href="/" onClick={ this.props.registro }> Registrar </a>
        </div>
      </form>
    );
  }
}
