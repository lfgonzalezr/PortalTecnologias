import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'
import FormularioLogin from './formulario-login.js';
import FormularioRegistro from './formulario-registro.js';
import { ValidateForm } from '../../utils/utils.js';
import  store from '../../utils/store.js';
import { LOGIN_SUCCESS } from '../../utils/actionCreator.js';
import { post } from '../../utils/utils.js';

import './login.css';

export default class Login extends Component {

  constructor(props){
    super(props);
    this.state = { formulario: 'login' };
  }

  onSubmitLogin = ( e ) => {
    e.preventDefault();
    this.onSubmit('formulario-login');
  }

  onSubmitRegistro = (e) => {
    e.preventDefault();
    this.onSubmit('formulario-registrar');
  }

  registro = ( e ) => {
    e.preventDefault();
    this.setState({formulario: 'registro'});
  }

  login = ( e ) => {
    e.preventDefault();
    this.setState({formulario: 'login'});
  }

  cancelar = (e) => {
    e.preventDefault();
    this.setState({formulario: 'redirect'});
  }

  onSubmit = ( nameForm ) => {
    var formularioRegistro = nameForm === 'formulario-registrar';
    var url = formularioRegistro ? '/admin/login/registrar' : '/admin/login/login' ;
    var validacion = ValidateForm(nameForm);
    if(!validacion.complete){
      var statusFormLabel = "Validar " + validacion.errors[0];
      this.setState({statusFormLabel: statusFormLabel, statusFormClass: "form-lbl-error active"});
      setTimeout( () => { this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"}); }, 4200);
      return;
    }
    if(formularioRegistro && validacion.json.password !== validacion.json.confirmPassword){
      this.setState({statusFormLabel: "La contraseña debe ser correcta", statusFormClass: "form-lbl-error active"});
      setTimeout( () => { this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"}); }, 4200);
      return;
    }
    post(url, validacion.json, (data) => {
      if(data.codigo === 0){
        store.dispatch(LOGIN_SUCCESS(data.contenido.nombreUsuario, data.contenido.token, data.contenido.correo, 1 ));
        this.setState({formulario: 'redirect'});
      } else {
        this.setState({statusFormLabel: data.mensage, statusFormClass: "form-lbl-error active"});
        setTimeout( () => { this.setState({statusFormLabel: "", statusFormClass: "form-lbl-error"}); }, 4200);
        return;
      }
    });
  }

  render(){
    const { formulario } = this.state;
    switch( formulario ) {
      case 'registro':
        return(
          <div className="pt-space-top margin-form">
            <div className="container-form-login">
              <FormularioRegistro onSubmit={ this.onSubmitRegistro } login={this.login} />
              <div className={ this.state.statusFormClass }>
                <label>{ this.state.statusFormLabel }</label>
              </div>
            </div>
          </div>
        );
      case 'login':
        return(
          <div className="pt-space-top margin-form">
            <div className="container-form-login">
              <FormularioLogin onSubmit={ this.onSubmitLogin } registro={ this.registro } cancelar={ this.cancelar} />
              <div className={ this.state.statusFormClass }>
                <label>{ this.state.statusFormLabel }</label>
              </div>
            </div>
          </div>
        );
      default:
        return (<Redirect to='/PortalTecnologias/' />);
      }
    }
  }
