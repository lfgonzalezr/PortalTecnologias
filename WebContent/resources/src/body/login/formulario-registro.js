import React, { Component } from 'react';
import Input from '../form/input.js';
import './login.css';

export default class FormularioRegistro extends Component {

  render(){
    return(
      <form onSubmit={ this.props.onSubmit } id="formulario-registrar" autoComplete="off" noValidate>
        <div className="form-group">
          <h2>Registro</h2>
        </div>
        <div className="form-group">
          <label>Usuario</label>
          <Input required="true" typeVal="alpha-numeric" placeholder="Usuario" name="nombreUsuario" readonly="false" />
        </div>
        <div className="form-group">
          <label>Correo</label>
          <Input required="true" typeVal="email" placeholder="Correo" name="correo" readonly="false" />
        </div>
        <div className="form-group">
          <label>Contraseña</label>
          <Input required="true" typeVal="alpha-numeric" placeholder="Contraseña" name="password" type="password" readonly="false" />
        </div>
        <div className="form-group">
          <label>Confirmar Contraseña</label>
          <Input required="true" typeVal="alpha-numeric" placeholder="Confirmar Contraseña" name="confirmPassword" type="password" readonly="false" />
        </div>
        <div className="form-group">
          <button type="submit" className="btn btn-success pt-btn-space"> Registrar </button>
          <button className="btn btn-secondary pt-btn-space" onClick={this.props.login}> Cancelar </button>
        </div>
      </form>
    );
  }
}
