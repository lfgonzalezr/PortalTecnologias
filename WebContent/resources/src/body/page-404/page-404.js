import React, { Component } from 'react';

export default class Page404 extends Component {

  render() {
    return(
      <div className="container pt-box-out-box pt-space-top">
        <div className="jumbotron pt-text-center">
          <h2>404</h2>
          <h2>
            <small>La pagina buscada no se encuentra disponible.</small>
          </h2>
        </div>
      </div>
    );
  }
}
