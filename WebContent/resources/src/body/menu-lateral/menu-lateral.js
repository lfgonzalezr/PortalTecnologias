import React, { Component } from 'react';
import './menu-lateral.css';

export default class MenuLateral extends Component{

  clickOnElement = ( item, e ) => {
    e.preventDefault();
    this.props.onClickElement(item.idMateria, item.idSeccion, 'element');
  }

  clickOnEditElement = (item, e) => {
    e.preventDefault();
    this.props.onClickElement(item.idMateria, item, 'edit');
  }

  clickOnNewElement = (item, e) => {
    e.preventDefault();
    this.props.onClickElement(item.idMateria, item, 'new');
  }

  render(){
     return(
       <div className="col-sm-2 pt-box-menu-container">
        <ul className="nav">
          <li className="nav-item" onClick={ this.props.regresar }>
            <a className="nav-link item-active" href="/"> Regresar </a>
          </li>
          {this.props.data.map( (item, index, array) => {
            if((array.length - 1) === index){
              return (
                <li className="nav-item" key={item.idSeccion} value={item.idSeccion} onClick={this.clickOnNewElement.bind(this, item)}>
                  <a className="nav-link item-active" href="/"> {item.titulo} </a>
                </li>);
            } else {
              return (
                <li className="menu-items" key={item.idSeccion} value={item.idSeccion}>
                  <a className="nav-link item-active menu-width" href="/" onClick={this.clickOnElement.bind(this, item)}> {item.titulo} </a>
                  <i className="glyphicon glyphicon-option-vertical menu-items-ico" onClick={this.clickOnEditElement.bind(this, item)}></i>
                </li>);
              }
            }
          )}
        </ul>
      </div>);
   }
}
