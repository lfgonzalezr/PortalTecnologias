
import { context, excFetchData } from './properties.js';
import store from './store';

export const post = (_url, _data, _fnSuccess) => {
  var path = context + _url;
  var token = store.getState().dataLogin.token;
  var body = JSON.stringify(_data);

  fetch(path , {
    method: 'POST',
    body: body,
    headers:{
      'Content-Type': 'application/json',
      'token': token
    }
  }).then((res) => {
    return res.json();
  })
  .catch((errData) => {
    console.log(errData);
    return excFetchData;
  })
  .then((response) => {

    _fnSuccess(response);

  });
}


export const ValidateForm = ( nameForm ) => {
  var elementsNoValid = [];
  var jsonValues = {};
  var elements = document.getElementById(nameForm).elements;
  for(var i = 0; i < elements.length ; i++){
    if(elements[i].className.indexOf("form-control") !== -1){
      if(elements[i].required && elements[i].value=== ""){
        elementsNoValid.push(elements[i].placeholder);
      } else {
        jsonValues[elements[i].name] = elements[i].value.trim().split(" ").filter((s)=>{return (s.length>0)?true:false;}).join(" ");
      }
    }
  }
  return {
    json: jsonValues,
    errors: elementsNoValid,
    complete: elementsNoValid.length > 0 ? false : true
  };
}
