export const LOGIN_SUCCESS = ( user, token, email, typeLogin ) =>{
  return {
    type: "LOGIN_SUCCESS",
    user: user,
    token: token,
    email: email,
    typeLogin: typeLogin
  };
}
