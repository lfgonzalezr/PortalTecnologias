import { createStore } from 'redux';

const reducer = (state, action) => {

  switch (action.type) {
    case "LOGIN_SUCCESS":
      return {
        ...state,
        dataLogin: {
          user: action.user,
          token: action.token,
          email: action.email,
          typeLogin: action.typeLogin
        }
      };
    default:
      return state;
  }
}

const dataSession = {
  dataLogin : {
    user: '',
    token: '',
    email: '',
    typeLogin: 0
  }
}

export default createStore(reducer, dataSession);
