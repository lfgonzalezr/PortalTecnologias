import React, { Component } from 'react';
import { Link, NavLink  } from 'react-router-dom';
/* import post */
import { post } from '../utils/utils.js';
/* import redux */
import store from '../utils/store';

export default class Header extends Component{

  constructor(props){
    super(props);
    this.state = ({ menuHeader: [] });
    this.menuActive = {
      "background": "#4a4a4a",
      "color": "#FFF !important"
    };

    store.subscribe( () => {
      var jsonSend = {
        correo: store.getState().dataLogin.email
      };
      post('/admin/menu/consultar-menu-header', jsonSend, ( data ) => {
        console.log(data);
        if(data.codigo === 0){
          var menuAvatar = {
            nombre: store.getState().dataLogin.user,
            uri: '/PortalTecnologias/usuario'
          };
          data.contenido.push(menuAvatar);
          this.setState({
            menuHeader: data.contenido
          });
        } else {
          console.log(data);
        }
      });
    });
  }

  componentDidMount() {
    post('/admin/menu/consultar-menu-header', {}, ( data ) => {
      if(data.codigo === 0){
        this.setState({
          menuHeader: data.contenido
        });
      } else {
        console.log(data);
      }
    });
  }

  render() {
    const { menuHeader } = this.state;
      return(
        <header>
          <nav className="navbar navbar-inverse navbar-fixed-top pt-header-zindex">
            <div className="container-fluid pt-box-out-box">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span className="icon-bar"></span> <span className="icon-bar"></span> <span className="icon-bar"></span>
                </button>
                <Link to="/PortalTecnologias" className="navbar-brand">BloG</Link>
              </div>
              <div className="collapse navbar-collapse" id="myNavbar">
                <ul className="nav navbar-nav navbar-right">
                {
                  menuHeader.map( (item, index) => {
                    return (
                      <li className="capitalize" key={ index }>
                        <NavLink to={item.uri} activeClassName="menu-active" activeStyle={this.menuActive}> { item.nombre } </NavLink>
                      </li>
                    );
                  })
                }
                </ul>
              </div>
            </div>
          </nav>
        </header>
      );
   }
}
