import React from 'react';
import ReactDOM from 'react-dom';
import Page from './page.js';

// ========================================
// Init
ReactDOM.render(
  <Page />,
  document.getElementById('root')
);
