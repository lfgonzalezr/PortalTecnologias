import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
/* Header */
import Header from './header/header.js';
/* Footer */
import Footer from './footer/footer.js';
/* Componentes */
import Home from './body/home/home.js';
import AdminHome from './body/admin/index.js';
import Page404 from './body/page-404/page-404.js';
import Login from './body/login/login.js';
import UsuarioComponent from './body/usuario/usuario-component';
import LogoutComponent from './body/logout/logout-component';

class Page extends Component{
   render(){
      return(
          <Router>
            <div>
              <Header />
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/PortalTecnologias" component={Home} />
                <Route path="/PortalTecnologias/admin" component={AdminHome} />
                <Route path="/PortalTecnologias/login" component={Login} />
                <Route path="/PortalTecnologias/usuario" component={UsuarioComponent} />
                <Route path="/PortalTecnologias/logout" component={LogoutComponent} />
                <Route component={Page404} />
              </Switch>
              <Footer />
            </div>
        </Router>
      );
   }
}
export default Page;
