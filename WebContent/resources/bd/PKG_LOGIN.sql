-- PACKAGE LOGIN/REGISTRO USUARIOS
CREATE OR REPLACE PACKAGE PKG_LOGIN IS
    TYPE RECORD_OUT IS REF CURSOR;
    --  DECLARACION LOGIN
    PROCEDURE LOGIN_USER (
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );
    -- DECLARACION REGISTRO

    PROCEDURE REGISTER_USER (
        USERNAME_IN   IN            NVARCHAR2,
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        STATUS_IN     IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );
    -- FUNCION PARA VALIDAR CORREO ELECTRONICO

    FUNCTION VALIDATE_EMAIL (
        E_MAIL NVARCHAR2
    ) RETURN NUMBER;

END;
/

-- CUERPO DEL PACKAGE

CREATE OR REPLACE PACKAGE BODY PKG_LOGIN IS
    --
    -- LOGIN DE USUARIOS
    --

    PROCEDURE LOGIN_USER (
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
        CORREO_VALUE     NVARCHAR2(50);
        PASSWORD_VALUE   NVARCHAR2(50);
    BEGIN
        IF ( VALIDATE_EMAIL(CORREO_IN) = 0 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'El correo ingresado no se encuentra registrado';
        ELSE
            SELECT U.FC_CORREO,
                   U.FC_PASSWORD
            INTO
                CORREO_VALUE,
                PASSWORD_VALUE
            FROM T_USUARIOS U
            WHERE UPPER(U.FC_CORREO) LIKE UPPER(CORREO_IN);

            IF ( PASSWORD_VALUE NOT LIKE PASSWORD_IN ) THEN
                CODE_OUT := 1;
                MSGE_OUT := 'Password incorrecto';
            ELSE
                OPEN DATA_OUT FOR SELECT U.FC_NOMBRE,
                                         U.FC_CORREO,
                                         TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI')
                                         || U.FI_ID_USUARIO AS TOKEN
                                  FROM T_USUARIOS U
                                  WHERE UPPER(U.FC_CORREO) LIKE UPPER(CORREO_IN);

                CODE_OUT := 0;
                MSGE_OUT := 'Login correcto';
            END IF;

        END IF;
    END LOGIN_USER;
    --
    -- REGISTRO DE USUARIOS
    --

    PROCEDURE REGISTER_USER (
        USERNAME_IN   IN            NVARCHAR2,
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        STATUS_IN     IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
        ID_USUARIO   NUMBER;
    BEGIN
        IF ( VALIDATE_EMAIL(CORREO_IN) > 0 ) THEN
            MSGE_OUT := 'Correo ya registrado';
            CODE_OUT := 1;
        ELSE
            ID_USUARIO := PKG_COMMONS.GET_ID('USUARIOS') + 1;
            INSERT INTO T_USUARIOS (
                FI_ID_USUARIO,
                FC_NOMBRE,
                FC_CORREO,
                FI_STATUS,
                FC_PASSWORD
            ) VALUES (
                ID_USUARIO,
                USERNAME_IN,
                CORREO_IN,
                STATUS_IN,
                PASSWORD_IN
            );

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Usuario registrado correctamente';
                OPEN DATA_OUT FOR SELECT U.FC_CORREO,
                                         U.FC_NOMBRE,
                                         TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI')
                                         || U.FI_ID_USUARIO AS TOKEN
                                  FROM T_USUARIOS U
                                  WHERE UPPER(CORREO_IN) = UPPER(U.FC_CORREO);

            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'No se registro el usuario correctamente';
            END IF;

        END IF;
    END REGISTER_USER;
    --
    -- FUNCION PARA VALIDAR CORREO ELECTRONICO

    FUNCTION VALIDATE_EMAIL (
        E_MAIL NVARCHAR2
    ) RETURN NUMBER IS
        COUNT_EMAIL   NUMBER := 0;
    BEGIN
        SELECT COUNT(*)
        INTO COUNT_EMAIL
        FROM T_USUARIOS U
        WHERE UPPER(E_MAIL) LIKE UPPER(U.FC_CORREO);

        RETURN COUNT_EMAIL;
    END VALIDATE_EMAIL;

END PKG_LOGIN;