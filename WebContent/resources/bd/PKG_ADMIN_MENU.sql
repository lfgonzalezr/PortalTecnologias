-- PACKAGE ADMINISTRACION DE MENUS
CREATE OR REPLACE PACKAGE PKG_ADMIN_MENU IS
    TYPE RECORD_OUT IS REF CURSOR;
    PROCEDURE CONSULTAR_MENU (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );

    PROCEDURE CONSULTAR_MENUS_POR_USUARIO (
        CORREO     IN         NVARCHAR2,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );

    PROCEDURE INSERTAR_MENU (
        NOMBRE          IN              NVARCHAR2,
        DESCRIPCION     IN              NVARCHAR2,
        URI             IN              NVARCHAR2,
        STATUS          IN              NUMBER,
        ICONO           IN              NVARCHAR2,
        TIPO_ELEMENTO   IN              NVARCHAR2,
        ORDEN           IN              NUMBER,
        CODE_OUT        OUT             NUMBER,
        MSGE_OUT        OUT             NVARCHAR2,
        DATA_OUT        OUT             RECORD_OUT
    );

    PROCEDURE ACTUALIZAR_MENU (
        ID_MENU         IN              NUMBER,
        NOMBRE          IN              NVARCHAR2,
        DESCRIPCION     IN              NVARCHAR2,
        URI             IN              NVARCHAR2,
        STATUS          IN              NUMBER,
        ICONO           IN              NVARCHAR2,
        TIPO_ELEMENTO   IN              NVARCHAR2,
        ORDEN           IN              NUMBER,
        CODE_OUT        OUT             NUMBER,
        MSGE_OUT        OUT             NVARCHAR2,
        DATA_OUT        OUT             RECORD_OUT
    );

    PROCEDURE BORRAR_MENU (
        ID_MENU    IN         NUMBER,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );

END;
/

CREATE OR REPLACE PACKAGE BODY PKG_ADMIN_MENU IS

    PROCEDURE CONSULTAR_MENU (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        CODE_OUT := 0;
        MSGE_OUT := 'Consulta correcta';
        OPEN DATA_OUT FOR SELECT *
                          FROM T_MENUS M
                          ORDER BY M.FI_ID_MENU DESC;

    END CONSULTAR_MENU;

    PROCEDURE CONSULTAR_MENUS_POR_USUARIO (
        CORREO     IN         NVARCHAR2,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.STR_NOT_EMPTY(CORREO) = 1 ) THEN
            CODE_OUT := 0;
            MSGE_OUT := 'Consulta correcta';
            OPEN DATA_OUT FOR SELECT TM.*
                              FROM T_ROLES TR
                              INNER JOIN T_ROLES_MENUS TRM ON TR.FI_ID_ROL = TRM.FI_ID_ROL
                              INNER JOIN T_MENUS TM ON TRM.FI_ID_MENU = TM.FI_ID_MENU
                              WHERE TR.FI_ID_ROL = 1
                                    AND TR.FI_STATUS = 1
                                    AND TM.FI_STATUS = 1;

        ELSE
            OPEN DATA_OUT FOR SELECT TM.*
                              FROM T_USUARIOS TU
                              INNER JOIN T_USUARIOS_ROLES TUR ON TU.FI_ID_USUARIO = TUR.FI_ID_USUARIO
                              INNER JOIN T_ROLES TR ON TUR.FI_ID_ROL = TR.FI_ID_ROL
                              INNER JOIN T_ROLES_MENUS TRM ON TR.FI_ID_ROL = TRM.FI_ID_ROL
                              INNER JOIN T_MENUS TM ON TRM.FI_ID_MENU = TM.FI_ID_MENU
                              WHERE UPPER(TU.FC_CORREO) LIKE UPPER(CORREO)
                                    AND TU.FI_STATUS = 1
                                    AND TR.FI_STATUS = 1
                                    AND TM.FI_STATUS = 1;

            CODE_OUT := 0;
            MSGE_OUT := 'Consulta correcta';
        END IF;
    END CONSULTAR_MENUS_POR_USUARIO;

    PROCEDURE INSERTAR_MENU (
        NOMBRE          IN              NVARCHAR2,
        DESCRIPCION     IN              NVARCHAR2,
        URI             IN              NVARCHAR2,
        STATUS          IN              NUMBER,
        ICONO           IN              NVARCHAR2,
        TIPO_ELEMENTO   IN              NVARCHAR2,
        ORDEN           IN              NUMBER,
        CODE_OUT        OUT             NUMBER,
        MSGE_OUT        OUT             NVARCHAR2,
        DATA_OUT        OUT             RECORD_OUT
    ) IS
        ID_MENU   NUMBER;
    BEGIN
        IF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(DESCRIPCION) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Descripcion';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(TIPO_ELEMENTO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: TipoElemento';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(ORDEN) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Orden';
        ELSE
            ID_MENU := PKG_COMMONS.GET_ID('MENUS') + 1;
            INSERT INTO T_MENUS (
                FI_ID_MENU,
                FC_NOMBRE,
                FC_DESCRIPCION,
                FC_URL,
                FI_STATUS,
                FC_ICONO,
                FI_TIPO_ELEMENTO,
                FI_ORDEN
            ) VALUES (
                ID_MENU,
                NOMBRE,
                DESCRIPCION,
                URI,
                STATUS,
                ICONO,
                TIPO_ELEMENTO,
                ORDEN
            );

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Menu registrado correctamente';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'El menu no se logro registrar correctamente';
            END IF;

        END IF;
    END INSERTAR_MENU;

    PROCEDURE ACTUALIZAR_MENU (
        ID_MENU         IN              NUMBER,
        NOMBRE          IN              NVARCHAR2,
        DESCRIPCION     IN              NVARCHAR2,
        URI             IN              NVARCHAR2,
        STATUS          IN              NUMBER,
        ICONO           IN              NVARCHAR2,
        TIPO_ELEMENTO   IN              NVARCHAR2,
        ORDEN           IN              NUMBER,
        CODE_OUT        OUT             NUMBER,
        MSGE_OUT        OUT             NVARCHAR2,
        DATA_OUT        OUT             RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.INT_NOT_EMPTY(ID_MENU) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Id Menu';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(DESCRIPCION) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Descripcion';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(TIPO_ELEMENTO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: TipoElemento';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(ORDEN) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Orden';
        ELSE
            UPDATE T_MENUS M
            SET M.FC_NOMBRE = NOMBRE,
                M.FC_DESCRIPCION = DESCRIPCION,
                M.FC_URL = URI,
                M.FI_STATUS = STATUS,
                M.FC_ICONO = ICONO,
                M.FI_TIPO_ELEMENTO = TIPO_ELEMENTO,
                M.FI_ORDEN = ORDEN
            WHERE M.FI_ID_MENU = ID_MENU;

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Menu registrado correctamente';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'El menu no se logro registrar correctamente';
            END IF;

        END IF;
    END ACTUALIZAR_MENU;

    PROCEDURE BORRAR_MENU (
        ID_MENU    IN         NUMBER,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.INT_NOT_EMPTY(ID_MENU) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Id Menu';
        ELSE
            DELETE FROM T_MENUS M
            WHERE M.FI_ID_MENU = ID_MENU;

            IF ( SQL%ROWCOUNT > 0 ) THEN
                CODE_OUT := 0;
                MSGE_OUT := 'Menu borrado correctamente';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'No se logro borrar el menu correctamente';
            END IF;

        END IF;
    END BORRAR_MENU;

END;
/