CREATE OR REPLACE PACKAGE PKG_ADMIN_USUARIOS IS
    TYPE RECORD_OUT IS REF CURSOR;
    -- CONSULTAR USUARIOS
    PROCEDURE CONSULTAR_USUARIOS (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );
    -- REGISTRAR USUARIO

    PROCEDURE REGISTRAR_USUARIO (
        NOMBRE        IN            NVARCHAR2,
        CORREO        IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );
    -- ACTUALIZAR USUARIO

    PROCEDURE ACTUALIZAR_USUARIO (
        ID_USUARIO    IN            NUMBER,
        NOMBRE        IN            NVARCHAR2,
        CORREO        IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );
    --BORRAR USUARIO

    PROCEDURE BORRAR_USUARIO (
        CORREO       IN           NVARCHAR2,
        CONTRASENA   IN           NVARCHAR2,
        CODE_OUT     OUT          NUMBER,
        MSGE_OUT     OUT          NVARCHAR2,
        DATA_OUT     OUT          RECORD_OUT
    );
    -- LOGIN USUARIO

    PROCEDURE LOGIN_USUARIO (
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );
    -- FUNCION PARA VALIDAR CORREO ELECTRONICO

    FUNCTION VALIDATE_EMAIL (
        E_MAIL NVARCHAR2
    ) RETURN NUMBER;

END;
/

CREATE OR REPLACE PACKAGE BODY PKG_ADMIN_USUARIOS IS

    --
    -- CONSULTA DE USUARIOS
    --

    PROCEDURE CONSULTAR_USUARIOS (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        OPEN DATA_OUT FOR SELECT U.FI_ID_USUARIO,
                                 U.FC_CORREO,
                                 U.FC_NOMBRE,
                                 U.FC_PASSWORD,
                                 U.FI_STATUS
                          FROM T_USUARIOS U;

        CODE_OUT := 0;
        MSGE_OUT := 'Consulta correcta';
    END CONSULTAR_USUARIOS;

    --
    -- INSERTAR USUARIO
    --

    PROCEDURE REGISTRAR_USUARIO (
        NOMBRE        IN            NVARCHAR2,
        CORREO        IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
        ID_USUARIO   NUMBER;
    BEGIN
        IF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(CORREO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Correo';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(PASSWORD_IN) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Password';
        ELSE
            IF ( VALIDATE_EMAIL(CORREO) > 0 ) THEN
                MSGE_OUT := 'Correo ya registrado';
                CODE_OUT := 1;
            ELSE
                ID_USUARIO := PKG_COMMONS.GET_ID('USUARIOS') + 1;
                INSERT INTO T_USUARIOS U (
                    U.FI_ID_USUARIO,
                    U.FC_CORREO,
                    U.FC_NOMBRE,
                    U.FC_PASSWORD,
                    U.FI_STATUS
                ) VALUES (
                    ID_USUARIO,
                    CORREO,
                    NOMBRE,
                    PASSWORD_IN,
                    STATUS
                );

                INSERT INTO T_USUARIOS_ROLES TR (
                    TR.FI_ID_USUARIO,
                    TR.FI_ID_ROL
                ) VALUES (
                    ID_USUARIO,
                    1
                );

                INSERT INTO T_TOKEN TT (
                    TT.FC_CORREO,
                    TT.FC_TOKEN,
                    TT.FI_STATUS,
                    TT.FD_FECHA
                ) VALUES (
                    CORREO,
                    TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI'),
                    1,
                    SYSDATE
                );

                IF ( SQL%ROWCOUNT > 0 ) THEN
                    COMMIT;
                    CODE_OUT := 0;
                    MSGE_OUT := 'Usuario insertado correctamente';
                    OPEN DATA_OUT FOR SELECT TU.FC_CORREO,
                                             TU.FC_NOMBRE,
                                             TT.FC_TOKEN   AS TOKEN
                                      FROM T_USUARIOS TU
                                      INNER JOIN T_TOKEN TT ON TU.FC_CORREO LIKE TT.FC_CORREO
                                      WHERE UPPER(TU.FC_CORREO) LIKE UPPER(CORREO);

                ELSE
                    CODE_OUT := 1;
                    MSGE_OUT := 'El usuario no se logro ingresar correctamente';
                END IF;

            END IF;
        END IF;
    END REGISTRAR_USUARIO;

    --
    -- ACTUALIZACION DE USUARIO
    --

    PROCEDURE ACTUALIZAR_USUARIO (
        ID_USUARIO    IN            NUMBER,
        NOMBRE        IN            NVARCHAR2,
        CORREO        IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.INT_NOT_EMPTY(ID_USUARIO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: idUsuario';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(CORREO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Correo';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(PASSWORD_IN) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Password';
        ELSE
            UPDATE T_USUARIOS U
            SET U.FC_NOMBRE = NOMBRE,
                U.FC_PASSWORD = PASSWORD_IN,
                U.FI_STATUS = STATUS
            WHERE UPPER(U.FC_CORREO) = UPPER(CORREO);

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Usuario insertado correctamente';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'El usuario no se logro ingresar correctamente';
            END IF;

        END IF;
    END ACTUALIZAR_USUARIO;
    --
    -- BORRADO DE USUARIO
    --

    PROCEDURE BORRAR_USUARIO (
        CORREO       IN           NVARCHAR2,
        CONTRASENA   IN           NVARCHAR2,
        CODE_OUT     OUT          NUMBER,
        MSGE_OUT     OUT          NVARCHAR2,
        DATA_OUT     OUT          RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.STR_NOT_EMPTY(CORREO) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Correo';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(CONTRASENA) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Contrasena';
        ELSE
            DELETE FROM T_USUARIOS U
            WHERE UPPER(U.FC_CORREO) = UPPER(CORREO)
                  AND UPPER(U.FC_PASSWORD) = UPPER(CONTRASENA);

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Usuario borrado correctamente';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'El usuario no se logro actualizar';
            END IF;

        END IF;
    END BORRAR_USUARIO;
    --
    -- LOGIN DE USUARIO
    --

    PROCEDURE LOGIN_USUARIO (
        CORREO_IN     IN            NVARCHAR2,
        PASSWORD_IN   IN            NVARCHAR2,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
        PASSWORD_VALUE   NVARCHAR2(50);
    BEGIN
        IF ( VALIDATE_EMAIL(CORREO_IN) = 0 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'El correo ingresado no se encuentra registrado';
        ELSE
            SELECT U.FC_PASSWORD
            INTO PASSWORD_VALUE
            FROM T_USUARIOS U
            WHERE UPPER(U.FC_CORREO) LIKE UPPER(CORREO_IN);

            IF ( PASSWORD_VALUE NOT LIKE PASSWORD_IN ) THEN
                CODE_OUT := 1;
                MSGE_OUT := 'Password incorrecto';
            ELSE
                UPDATE T_TOKEN TT
                SET
                    TT.FI_STATUS = 0
                WHERE UPPER(TT.FC_CORREO) LIKE UPPER(CORREO_IN);

                INSERT INTO T_TOKEN TT (
                    TT.FC_TOKEN,
                    TT.FC_CORREO,
                    TT.FI_STATUS,
                    TT.FD_FECHA
                ) VALUES (
                    TO_CHAR(SYSDATE, 'YYYYMMDDHH24MI'),
                    CORREO_IN,
                    1,
                    SYSDATE
                );

                IF ( SQL%ROWCOUNT > 0 ) THEN
                    COMMIT;
                    CODE_OUT := 0;
                    MSGE_OUT := 'Login correcto';
                    OPEN DATA_OUT FOR SELECT TU.FC_CORREO,
                                             TU.FC_NOMBRE,
                                             TT.FC_TOKEN   AS TOKEN
                                      FROM T_USUARIOS TU
                                      INNER JOIN T_TOKEN TT ON TU.FC_CORREO LIKE TT.FC_CORREO
                                      WHERE UPPER(TU.FC_CORREO) LIKE UPPER(CORREO_IN);

                END IF;

            END IF;

        END IF;
    END LOGIN_USUARIO;
    --
    -- FUNCION PARA VALIDAR CORREO ELECTRONICO
    --

    FUNCTION VALIDATE_EMAIL (
        E_MAIL NVARCHAR2
    ) RETURN NUMBER IS
        COUNT_EMAIL   NUMBER := 0;
    BEGIN
        SELECT COUNT(*)
        INTO COUNT_EMAIL
        FROM T_USUARIOS U
        WHERE UPPER(E_MAIL) LIKE UPPER(U.FC_CORREO);

        RETURN COUNT_EMAIL;
    END VALIDATE_EMAIL;

END PKG_ADMIN_USUARIOS;