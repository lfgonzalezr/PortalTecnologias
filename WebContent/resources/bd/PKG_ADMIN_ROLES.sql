CREATE OR REPLACE PACKAGE PKG_ADMIN_ROLES IS
    TYPE RECORD_OUT IS REF CURSOR;
    PROCEDURE CONSULTAR_ROLES (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );

    PROCEDURE INSERTAR_ROL (
        NOMBRE        IN            NVARCHAR2,
        DESCRIPCION   IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );

    PROCEDURE ACTUALIZAR_ROL (
        ID_ROL        IN            NUMBER,
        NOMBRE        IN            NVARCHAR2,
        DESCRIPCION   IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    );

    PROCEDURE BORRAR_ROL (
        ID_ROL     IN         NUMBER,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    );

END;
/

CREATE OR REPLACE PACKAGE BODY PKG_ADMIN_ROLES IS

    PROCEDURE CONSULTAR_ROLES (
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        OPEN DATA_OUT FOR SELECT R.FI_ID_ROL,
                                 R.FC_NOMBRE,
                                 R.FC_DESCRIPCION,
                                 R.FI_STATUS
                          FROM T_ROLES R;

    END CONSULTAR_ROLES;

    PROCEDURE INSERTAR_ROL (
        NOMBRE        IN            NVARCHAR2,
        DESCRIPCION   IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
        ID_ROL   NUMBER;
    BEGIN
        IF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(DESCRIPCION) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Descripcion';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSE
            ID_ROL := PKG_COMMONS.GET_ID('ROLES') + 1;
            INSERT INTO T_ROLES R (
                R.FI_ID_ROL,
                R.FC_NOMBRE,
                R.FC_DESCRIPCION,
                R.FI_STATUS
            ) VALUES (
                ID_ROL,
                NOMBRE,
                DESCRIPCION,
                STATUS
            );

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Rol creado de forma correcta';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'No se logro crear el rol';
            END IF;

        END IF;
    END INSERTAR_ROL;

    PROCEDURE ACTUALIZAR_ROL (
        ID_ROL        IN            NUMBER,
        NOMBRE        IN            NVARCHAR2,
        DESCRIPCION   IN            NVARCHAR2,
        STATUS        IN            NUMBER,
        CODE_OUT      OUT           NUMBER,
        MSGE_OUT      OUT           NVARCHAR2,
        DATA_OUT      OUT           RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.INT_NOT_EMPTY(ID_ROL) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: idRol';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(NOMBRE) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Nombre';
        ELSIF ( PKG_COMMONS.STR_NOT_EMPTY(DESCRIPCION) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Descripcion';
        ELSIF ( PKG_COMMONS.INT_NOT_EMPTY(STATUS) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: Status';
        ELSE
            UPDATE T_ROLES R
            SET R.FC_NOMBRE = NOMBRE,
                R.FC_DESCRIPCION = DESCRIPCION,
                R.FI_STATUS = STATUS
            WHERE R.FI_ID_ROL = ID_ROL;

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Rol actualizado de forma correcta';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'No se logro actualizar el rol';
            END IF;

        END IF;
    END ACTUALIZAR_ROL;

    PROCEDURE BORRAR_ROL (
        ID_ROL     IN         NUMBER,
        CODE_OUT   OUT        NUMBER,
        MSGE_OUT   OUT        NVARCHAR2,
        DATA_OUT   OUT        RECORD_OUT
    ) IS
    BEGIN
        IF ( PKG_COMMONS.INT_NOT_EMPTY(ID_ROL) = 1 ) THEN
            CODE_OUT := 1;
            MSGE_OUT := 'Falta dato: idRol';
        ELSE
            DELETE FROM T_ROLES R
            WHERE R.FI_ID_ROL = ID_ROL;

            IF ( SQL%ROWCOUNT > 0 ) THEN
                COMMIT;
                CODE_OUT := 0;
                MSGE_OUT := 'Rol borrado de forma correcta';
            ELSE
                CODE_OUT := 1;
                MSGE_OUT := 'No se logro borrar el rol';
            END IF;

        END IF;
    END BORRAR_ROL;

END PKG_ADMIN_ROLES;